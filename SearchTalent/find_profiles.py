# -*- coding: utf-8 -*-
"""
Created on Tue Sep 18 17:29:27 2018

@author: Jim Sellmeijer
"""
#import pdb
import sys
import warnings
import socket
#import pickle
import requests
import re
import pandas as pd
import numpy as np
import json
import string
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from utils import config as cfg
from utils.indeed_utils import get_indeed_profiles #get_candidate_list, get_candidate_infor
from utils.github_utils import get_github_profiles
from utils.tools import get_matches_from_zoho
from utils.xing_utils import google_for_profiles
from utils.geography import get_language
sys.path.append("..")
warnings.simplefilter("ignore", UserWarning)
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
regexp_date = re.compile('([\d/\d])+')
regexp_no = re.compile('([\d])')
        
class summary:

    def __init__(self,ID):
        self.summs = {}
        self.socket = None
        self.current_source = ''
        self.summs['id'] = ID
    
    def add_source(self, source, amount=0,email_amount=0):
        
        if source in self.summs.keys():
            self.summs[source] = {cfg.AMOUNT:self.summs[source][cfg.AMOUNT]+amount,'email':self.summs[source]['email']+email_amount}
        else:
            self.summs[source] = {cfg.AMOUNT:amount,'email':email_amount}
        self.current_source = source    
        self.summary = str.encode(json.dumps(self.summs))
        
    def set_socket(self,socket):
        self.socket = socket
        
    def send_message(self,message = None):
        if self.socket:
            if message:
                mess = message+cfg.SOCK_MES_END
#                print(mess)
                self.socket.sendall(mess)
            else:
                mess = self.summary+cfg.SOCK_MES_END
#                print(mess)
                self.socket.sendall(mess)
#                self.socket.sendall('hoi'.encode()+cfg.SOCK_MES_END)

def main(job_title,city,skills,no_people,ID='0',sock = False):
    """
    param job_title: string of job title
    param city: string of city
    param skills: string of required skills seperated by comma
    param no_people: int of how many people we need
    param ID: string of job ID
    param sock: bool of whether or not we should communicate over socket
    return: dataframe with data from all sources
    """
    dfs = []
    #remove text between brackets, remove punctuation and spaces
    job_title = re.sub("([\(\[]).*?([\)\]])", "\g<1>\g<2>", job_title).translate(str.maketrans('','',string.punctuation)).strip()
    summary_ = summary(ID)
    #setup socket
    if sock:
        #server socket
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serversocket.connect((cfg.SOCKET_HOST, cfg.SOCKET_PORT))
        serversocket.sendall(str.encode('I am running! '))
        summary_.set_socket(serversocket)
    else:
        summary_ = False
    #first see if we have ppl in our zoho db
    items_df_db = get_matches_from_zoho(skills, city, summary_)
    dfs.append(items_df_db)
    print(str(len(items_df_db))+' people found in db')
    if summary_:
        summary_.send_message()
    #get github data:
    items_df_gh = get_github_profiles(skills,city,summary_)
    dfs.append(items_df_gh)
    print(str(len(items_df_gh))+' people found on gh')
    if summary_:
        summary_.send_message()
    #get indeed data:
    if get_language(city)=='en':
        items_df_indeed = get_indeed_profiles(job_title, city, summary_)
        dfs.append(items_df_indeed)
        print(str(len(items_df_indeed))+' people found on indeed')          
        if summary_:
            summary_.send_message()
    #then substract how many you found from no_people and continue here
    no_people = no_people-np.sum([len(df) for df in dfs])#(len(items_df_db)+len(items_df_gh)+len(items_df_indeed))
    items_df_google = google_for_profiles(job_title, skills, city, no_people, summary_)
    dfs.append(items_df_google)
    if summary_:
        summary_.send_message()
    #concatenate all items_df's    
    all_items_df = pd.concat(dfs,axis=0, ignore_index=True)
    print(str(len(all_items_df))+' people found in total')    
    #save the data locally
    all_items_df.to_csv('all_items_df.csv')
    #turn items_df to dict
    items_dict = all_items_df.fillna('').to_dict(orient='index')
    items_dict = {ind: items_dict[key] for ind,key in enumerate(items_dict.keys())}
    items_dict = {ID:items_dict}
    #turn items_dict to json
    items_json = json.dumps(items_dict).encode()   
    if summary_:
        summary_.send_message(items_json)
    return all_items_df,items_json    

if __name__=='__main__':
    #run main function
#    all_items_df = main('Frontend developer','london','python,css',500,'007',True)
    all_items_df = main('Frontend developer','London','Javascript, css',200,'007',False)
