# -*- coding: utf-8 -*-
"""
    SEARCH TALENT DATA SCIENCE TEAM
"""
import os
from random import choice
from copy import deepcopy
from utils.data import get_api, get_attachment, get_associated_candidates, filter_jobs, update_multiple_records, \
    broadcast_translation, detect_language, translate, get_previous_app, clean_string,\
    get_job_ads, get_candidates, get_clients, get_interviews,get_attachments,save_db_attachment, get_associations
from utils.config import CANDIDATES, CANDIDATEID, CLIENTS, CLIENTID, CLIENT_NAME, JOBOPENING, JOBOPENINGID, \
    CONTACTED_JOB,  INTERVIEW, INTERESTS, GET_RECORDS, FORMAL, POSTING_TITLE, LANGUAGE, LANGUAGES, \
    LANGUAGE_CODES, DEFAULT_CITY, DEFAULT_LANGUAGE, ID, SKILL_SET, BUTTON, CLICKED_BUTTON, PREVIOUS_APP, \
    EMAIL, LAST_EMAIL, ROBOT_EMAIL, TIME, CITY, PDF_PATH, PHONE,SOURCE, FILE
from utils.geography import get_local_time, get_language, get_current_time
from utils.topic_classification.get_word2vec import get_top_interests
from utils.new_email_compiler import generate_email, send_email,  send_sms
import langcodes

#import threading
debug = True #set if we are testing the script
use_dbase = True #set if we take data from postgres db or zoho

if __name__ == '__main__':

#def main():
    if debug == True:
        wait_time = 60
    else:
        wait_time = 300
    print("Begin new")
    # Retrieve all records
    if use_dbase:
        all_jobs = get_job_ads()
        #get a dictionary with all the jobs and the associated candidates
#        associated_cand_dict, ass_data = get_associations(all_jobs)
        #feed the data with associations to the candidates so we can extract
        #whether someone has already been contacted
        all_candidates =  get_candidates()
        associated_cand_dict, all_candidates = get_associations(all_jobs, all_candidates)

        all_clients = get_clients()
#        all_contacts = get_contacts(all_clients)
        all_interviews = get_interviews()
        all_attachments = get_attachments()
    else:
        all_candidates = get_api(CANDIDATES, GET_RECORDS, CANDIDATEID)
        all_clients = get_api(CLIENTS, GET_RECORDS, CLIENTID)
        all_jobs = get_api(JOBOPENING, GET_RECORDS, JOBOPENINGID)
#        all_contacts = get_api(CONTACTS, GET_RECORDS, CONTACTID)
        all_interviews = get_api(INTERVIEW, GET_RECORDS, CANDIDATEID)

        # Keep jobs whose Opening Status field is In-Progress
        relevant_job_ids = filter_jobs(all_jobs)
        # Get associated candidates for each job
        associated_cand_dict = {job_id: get_associated_candidates(job_id) for job_id in relevant_job_ids}

    update_candidates = {}
    update_jobs = {}
    #collect job IDs and associated candidates
    for job_ID, candidate_IDs in associated_cand_dict.items():
        
        #instantiate update dictionary
        update_job = {ID: job_ID}
        #get job and client records
        job_record = deepcopy(all_jobs[job_ID])
        client_record = all_clients[job_record[CLIENTID]]
        
#        #skip when there is no client for the job
#        if job_record[CONTACTID] not in all_contacts:
#            continue
        
        #collect contact record
#        contact_record = all_contacts[job_record[CONTACTID]]
        
        #generate job ad pdf file name
        path = "{} {}_job_ads.pdf".format(job_record[POSTING_TITLE], all_clients[job_record[CLIENTID]][CLIENT_NAME])
#        path = "{} {}_job_ads.pdf".format(job_record[POSTING_TITLE], job_record[CLIENT_NAME])
        
        #get job ad pdf
        if use_dbase:
            try:
                save_db_attachment(job_ID, path, all_attachments)
                job_record[FILE] = True
            except:
                pass
        else:
            try:
                get_attachment(job_ID, path)
                job_record[FILE] = True
            except:
                pass
        
        #when there is a file, add the path to the record
        if os.path.isfile(path):
            job_record[PDF_PATH] = path
        else:
            job_record[PDF_PATH] = None
            
#        #collect contact's email sig    
#        if EMAIL_SIGNATURE not in contact_record:
#            contact_record[EMAIL_SIGNATURE] = ""
        
        #replace missing values with none
        job_record[FORMAL] = "informal" if client_record.get(FORMAL) == "false" else 'formal'
        job_record[INTERESTS] = client_record.get(INTERESTS)
        job_record["custom job offer text"] = job_record.get("custom job offer text")
        job_record["Job Description"] = job_record.get("Job Description")
        
        #translate languages when necessary (hard skills)
        job_record, new_fields = broadcast_translation(job_record, "Hard Skills")
        #add the new fields (translation) to the update job
        for new_field in new_fields:
            update_job[new_field] = job_record[new_field]
        
        #translate languages when necessary (soft skills)        
        job_record, new_fields = broadcast_translation(job_record, "Soft Skills")
        for new_field in new_fields:
            update_job[new_field] = job_record[new_field]
        
        #skip when hard skills are not present after translation
        if 'Hard Skills en' not in job_record:
            continue
        
        #turn language to default language when there is no language
        if LANGUAGE not in job_record:
            job_record[LANGUAGE] = detect_language(job_record[POSTING_TITLE]) if job_record[
                                                                                     POSTING_TITLE] != None else DEFAULT_LANGUAGE
        #turn language to default when language is not present
        if job_record[LANGUAGE] not in LANGUAGE_CODES:
            job_record[LANGUAGE] = DEFAULT_LANGUAGE
        
        #add language to update job
        update_job[LANGUAGE] = job_record[LANGUAGE]
        #add the complete update job to common update dictionary
        update_jobs[job_ID] = update_job
        
        #loop through candidates
        candidate_count = 0
        for candidate_ID in candidate_IDs:
            #collect candidate record
            candidate_record = deepcopy(all_candidates[candidate_ID])
            
            #skip candidate when there is no email or when is already emailed
            if candidate_record.get(CONTACTED_JOB) and job_ID in candidate_record.get(CONTACTED_JOB) or candidate_record.get(EMAIL) == None:
                continue
            #decide whether candidate receives robot email
            is_robot = choice(["true", "false"])
            
            #instantiate update candidate
            update_candidate = {ID: candidate_ID}
            #add ids to button URL
            button = BUTTON.format(job_ID, candidate_ID)
            #turn missing values to None
            candidate_record[SKILL_SET] = candidate_record.get(SKILL_SET)
            candidate_record[INTERESTS] = candidate_record.get(INTERESTS)
            
            if candidate_record[INTERESTS]:
                #translate when interests are not in english to english
                if detect_language(candidate_record[INTERESTS]) != "en":
                    candidate_record[INTERESTS] = ", ".join(translate([candidate_record[INTERESTS]], DEFAULT_LANGUAGE))
                
                #use word to vector model to get the candidate interests' topics
                candidate_record[INTERESTS] = ", ".join(
                    get_top_interests(candidate_record[INTERESTS].lower().split(',')))
                
            #collect application history from interviews
            candidate_record[PREVIOUS_APP] = get_previous_app(all_interviews.get(candidate_ID))
            
            candidate_record[ROBOT_EMAIL] = is_robot
            
            #when we have the city we will get the local time
            if CITY in candidate_record:
                candidate_record[TIME] = get_local_time(candidate_record[CITY])
            #when not, we take the default time
            else:
                candidate_record[TIME] = get_local_time(DEFAULT_CITY)
            
            #get the language of the city the person lives in
            #check if languages is filled
            if LANGUAGE not in candidate_record:
                #if not, check if there is a languages field so we can take that language
                if LANGUAGES in candidate_record:
                    #translate to english
                    lang_text = translate([candidate_record[LANGUAGES]],DEFAULT_LANGUAGE)[0].split(',')[0]
                    try:
                        candidate_record[LANGUAGE] = langcodes.find(lang_text).language
                    except Exception:
                        candidate_record[LANGUAGE] = DEFAULT_LANGUAGE
                else:
                    #if not, we take the city language
                    candidate_record[LANGUAGE] = get_language(candidate_record[CITY])
            
            #check if there is a phone number to send sms
            if PHONE in candidate_record.keys():
                #generate the email
                email, message = generate_email(candidate_record, client_record, deepcopy(job_record), button,'sms')                    
                message_sent = send_sms(candidate_record[PHONE], message)
            #when there is no phone number we send email
            else:
                email, message = generate_email(candidate_record, client_record, deepcopy(job_record), button)                    
                message_sent = send_email(email, candidate_record)

            #when the email is sent, store in update candidate
            if message_sent:
                value = clean_string("""{}/{}/{}""".format(candidate_ID, get_current_time(), message))
                update_candidate[LAST_EMAIL] = value
                print('email sent to ' +candidate_record[EMAIL] )
            else:
                update_candidate[LAST_EMAIL] = "Email address does not exist."
            
            #set candidate source to database:
            #after the first email, the source is set to Data Base, as they are now, not new anymore
            update_candidate[SOURCE] = "Data Base"
            
            #reset click button to empty
            update_candidate[CLICKED_BUTTON] = ""

            #add the job ID to the candidate record
            if CONTACTED_JOB in candidate_record:
                candidate_record[CONTACTED_JOB] = "{},{}".format(candidate_record[CONTACTED_JOB], job_ID)
            else:
                candidate_record[CONTACTED_JOB] = job_ID

            #add fields to update candidate
            update_candidate[ROBOT_EMAIL] = candidate_record[ROBOT_EMAIL]
            update_candidate[LANGUAGE] = candidate_record[LANGUAGE]
            update_candidate[CONTACTED_JOB] = candidate_record[CONTACTED_JOB]
            update_candidate[ID] = candidate_ID
            #update the candidate's record in all candidates
            all_candidates[candidate_ID] = candidate_record

            #add update candidate to complete update candidates list
            update_candidates[candidate_ID] = update_candidate
            candidate_count += 1

            #do api call to update when we reach 100 candidates
            if candidate_count == 100:
                while True:
                    try:
                        update_multiple_records(CANDIDATES, update_candidates)
                        print("Finish updating 100 records!")
                    except:
                        continue
                    break
                candidate_count = 0
                update_candidates = {}
    while True:
        try:
            #update the last batch
            update_multiple_records(CANDIDATES, update_candidates)
            #update all job openings
            update_multiple_records(JOBOPENING, update_jobs)
            print("Finish updating remaining records!")
        except:
            continue
        break

#    threading.Timer(wait_time, main).start()

#
#if __name__ == '__main__':
#    main()
