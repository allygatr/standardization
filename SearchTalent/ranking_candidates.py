# -*- coding: utf-8 -*-
"""
Created on Wed Aug  1 11:59:01 2018

@author: Jim Sellmeijer
"""
import pandas as pd
#import numpy as np
import urllib3
http = urllib3.PoolManager()

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.encoders import encode_base64
from utils.data import get_api,get_current_time, update_multiple_records,get_job_ads,\
get_contacts, get_candidates, get_associations,get_clients,get_associated_candidates
import utils.config as cfg
import utils.email_template as tmp

               
import copy
import datetime
from bs4 import BeautifulSoup


from utils.new_email_compiler import send_email, send_sms

debug = True
exclude = [']', '}', '{', '[',  '>',  '|',  '<', '/', '\\']
wait_time = 86400

def extract_answers_from_candidates(ja_info,job_candidates):
#    q_strings = ['q1','q2','q3','q4','sal']
     
    ans_dic = {}
    #loop through ids
    for can_ID in job_candidates.keys():
        try:
            lang = job_candidates[can_ID][cfg.LANGUAGE]

            if lang!='en':
                
                #get the string for the skill from each of the two languages
                english_string = ja_info[cfg.HARD_SKILLS_EN]+','+ja_info[cfg.SOFT_SKILLS_EN]
                english_skills = [sk.strip() for sk in english_string.split(',')]
                other_string = ja_info['Hard Skills '+lang]+','+ja_info['Soft Skills '+lang]
                other_skills = [sk.strip() for sk in other_string.split(',')]                
                
                #replace the string of the other language with the english ones
                string = job_candidates[can_ID][cfg.QUESTIONNAIRE_RESULTS]
                for ind,sk in enumerate(other_skills):
                    string = string.replace(sk,english_skills[ind])
                                
            else:
                
                string = job_candidates[can_ID][cfg.QUESTIONNAIRE_RESULTS]
            
            ans_dic[can_ID] = {}
            substrings = string.split('/')[1].split(',')
            for substring in substrings:
                ans_dic[can_ID][substring[:substring.find(':')]]=int(substring[substring.find(':')+1:])
        except Exception:
            string = None

    #turn to df            
    df = pd.DataFrame(ans_dic).T
    names = ['']*len(df)
    for ind,ID in enumerate(df.index):
        names[ind] = job_candidates[ID][cfg.FIRST_NAME] + ' ' + job_candidates[ID][cfg.LAST_NAME]
    
    df.insert(0,'name',names)#df['name'] = names    
    
    
    return df

def rank_candidates(df):
    
#    #put together a dictionary with importance values
#    q_imp = {}
#    q_imp['q1'] = 25    
#    q_imp['q2'] = 5    
#    q_imp['q3'] = 15    
#    q_imp['q4'] = 5
#    q_imp['sal'] = 50
    
    keys=[key for key in df.columns if key!='name']
    
    #if there are no results, then we do nothing
    if len(keys)>0:
        #by doing this salary is equally important as all of the questions from the quesionnaire
        sal_imp = len([key for key in keys if key!='sal'])
        
        for key in keys:
            #normalize and multiply against importance
            df[key]=[(1-float(i)/max(df[key]))*sal_imp if key=='sal'  else float(i)/max(df[key]) for i in df[key] ]
        df['score']=df.sum(axis = 1)
        df['score'] = [int(float(i)/max(df['score'])*100) for i in df['score']]
        #sort by score
        df = df.sort_values('score',ascending = False)
        
        df.columns = [key + ' score' if key !='score' and key !='name' else key for key in df.columns]
    
    return df

def rank_2_text(df_ranked):
    rank_text = ''
    
    
    if 'score' in df_ranked.columns:
        rank_text = '\n'.join([str(ind+1)+'. '+str(person) + ' '+str(df_ranked.at[person,'score']) for ind,person in enumerate(df_ranked.index)])
    else:
        print('no score in ranking pdf')
    return rank_text


def generate_text(contactInfo):
    text = '''<p>Dear {},</p>
    <p>Here is the ranking list for the {} at {}. You can also find it in zoho.
    The csv file is attached. </p>
    <p>Kind regards,</p>
    <p>The Searchtalent team</p>'''.format(contactInfo[cfg.FIRST_NAME],contactInfo[cfg.JOB_TITLE], contactInfo['Client Name'])
    return text



def generate_email(message,title, email_ad, attachment):
    email = MIMEMultipart()
    email['Subject'] = title
    # email['From'] = Searchtalent Team
    email['To'] = email_ad
    text = MIMEText(message, 'html')
    email.attach(text)
    # attach the csv file
    
    if attachment:
        with open(attachment, "rb") as opened:
            openedfile = opened.read()
        attachedfile = MIMEApplication(openedfile, _subtype="pdf", _encoder=encode_base64)
        # define the name of the attachment (job title + company .pdf)
        attachedfile.add_header('content-disposition', 'attachment',
                                filename='ranking.csv')
        email.attach(attachedfile)
    return email

def send_results_to_recruiter(contact_info,file_name):
    #make message
    message = generate_text(contact_info)
    #make email object
    email = generate_email(message,'Candidate list is uploaded to Zoho', contact_info['Email'],file_name)
    #send it to the contact
    send_email(email, contact_info)
    


def compile_reminder(formal,job_opening,contact_info,all_can_no_ques,url):
    
#    emails = []
    update_list = {}
    #get recruiter information
    #get name of candidate
    
    for can_ID in all_can_no_ques.keys():
        
        update_dict = {}
        
        #get candidate language
        try:
            lang = all_can_no_ques[can_ID][cfg.LANGUAGE]
        except Exception:
            lang= 'en'
        
        #determine formal
        if formal.lower()=='false':
            formal='informal'
        else:
            formal = 'formal'
        
        #get reminder text
        if lang in cfg.FORMAL_LANGUAGES:
            reminder = tmp.REMINDER[lang][formal]
        else:
            reminder =tmp.REMINDER[lang]
        
        #get first name
        if cfg.FIRST_NAME in all_can_no_ques[can_ID].keys():
            name = all_can_no_ques[can_ID]['First Name']
        else:
            name = 'sir or madam'

        # if the phone number of candidate is available, his or her email address is available, we send email reminder
        if cfg.PHONE in all_can_no_ques[can_ID].keys() and cfg.EMAIL in all_can_no_ques[can_ID].keys() :
            field = 'sms'
            b_text = '<a href="[url]">'+tmp.HERE[lang]+'</a>'
        # if the phone number of candidate is available, his or her email address is not available, we send sms
        elif cfg.PHONE in all_can_no_ques[can_ID].keys() and cfg.EMAIL not in all_can_no_ques[can_ID].keys():
            field = 'sms'
            b_text = tmp.HERE[lang] + ': [url]'
        # if the phone number of candidate is not available, his or her email address is available, we send email reminder
        elif cfg.PHONE not in all_can_no_ques[can_ID].keys() and cfg.EMAIL in all_can_no_ques[can_ID].keys():
            field = 'email'
            b_text = '<a href="[url]">'+tmp.HERE[lang]+'</a>'
        else:
            continue

        text = '<p>'+ tmp.INTRODUCTION_GREETING[lang]['personal'][0] + ' [name],</p><p>'+reminder.replace('[email]', tmp.EMAIL[lang][field]).replace('[button]', tmp.BUTTON[lang][field]).replace('[here]',b_text)+\
        '</p><p>'+tmp.CONCLUSION['closing']['no_weekend'][lang][0]+\
        '''</p>
        <p>[recruiter_name]</p>
        [recruiter_sig]'''

        text = text.replace('[name]',name.split()[0])#candidate name
        text = text.replace('[position]',job_opening[cfg.POSTING_TITLE]) #posting title
        text = text.replace('[company]',job_opening[cfg.CLIENT_NAME]) #company name
        text = text.replace('[url]',url +ja_ID+'/'+can_ID)
        text = text.replace('[recruiter_name]',cfg.RECRUITER_FNAME+' '+cfg.RECRUITER_SNAME) #recruiter name

        # send email to zoho:
        soup = BeautifulSoup(text)
        message = soup.get_text()
        # remove characters that zoho won't take
        message = ''.join([char for char in message if char not in exclude])


        if cfg.EMAIL in all_can_no_ques[can_ID].keys():
            try:
                text = text.replace('[recruiter_sig]',contact_info[cfg.RECRUITER_SIG]) #recruiter signature
            except Exception:
                text = text.replace('[recruiter_sig]','') #recruiter signature
            # put the dict in the list
            update_list[can_ID] = update_dict

            # compile email
            email = generate_email(text, 'Questionnaire ' + job_opening[cfg.POSTING_TITLE],
                                   all_can_no_ques[can_ID][cfg.EMAIL], None)
#            emails.append(email)
            # send the email
            send_email(email, all_can_no_ques[can_ID])
        elif cfg.PHONE in all_can_no_ques[can_ID].keys():
            send_sms(all_can_no_ques[can_ID][cfg.PHONE], message)
#            emails.append(message)
        else:
            continue

        # send the email to zoho
        update_dict[cfg.EMAIL_REMINDER] = message
        update_dict[cfg.ID] = can_ID
        update_dict[cfg.REMINDER_TIME] = get_current_time()


    #do the update
    update_multiple_records(cfg.CANDIDATES, update_list)
    
#    return emails
    

if __name__ == '__main__':    
#def main():       
    
    today = datetime.datetime.today()
    
    use_dbase = False
    
    run = True
    
   #get the data
    if use_dbase:
        all_jobs = get_job_ads()
        #feed the data with associations to the candidates so we can extract
        #whether someone has already been contacted
        all_candidates =  get_candidates()
        associated_cand_dict = get_associations(all_jobs, all_candidates, True)[0]
        all_clients = get_clients()
        all_contacts = get_contacts(all_clients)
    else:
        all_candidates = get_api(cfg.CANDIDATES, cfg.GET_RECORDS, cfg.CANDIDATEID)
        all_clients = get_api(cfg.CLIENTS, cfg.GET_RECORDS, cfg.CLIENTID)
        all_jobs = get_api(cfg.JOBOPENING, cfg.GET_RECORDS, cfg.JOBOPENINGID)
        all_contacts = get_api(cfg.CONTACTS, cfg.GET_RECORDS, cfg.CONTACTID)


        # Get associated candidates for each job
        associated_cand_dict = {job_id: get_associated_candidates(job_id) for job_id in all_jobs.keys()}
        
    update_jobs = {}
    #loop through the job openings
    for ja_ID in all_jobs.keys():
        update_job = {cfg.ID: ja_ID}

        
        if  cfg.CONTACTID not in all_jobs[ja_ID].keys() or all_jobs[ja_ID][cfg.JOBOPENINGSTATUS]!='In-progress':
            continue
        
        contact_info = all_contacts[all_jobs[ja_ID][cfg.CONTACTID]]
        contact_info[cfg.CLIENT_NAME] = all_jobs[ja_ID][cfg.CLIENT_NAME]            

        #check if a target date is registered
        if cfg.TARGET_DATE in all_jobs[ja_ID].keys() and cfg.RANKING not in all_jobs[ja_ID].keys():
            
            target_date = all_jobs[ja_ID][cfg.TARGET_DATE]
            if target_date =='None' or None:
                continue
            
            #split deadline date text
            split_date = all_jobs[ja_ID][cfg.TARGET_DATE].split('-')
            #turn it into datetime
            deadline = datetime.date(int(split_date[0]),int(split_date[1]),int(split_date[2][:2]))        
            #get the associated candidate IDs
            can_IDs = associated_cand_dict[ja_ID]
            #if can_IDs is not empty
            if can_IDs:
                
                job_candidates = {can_ID:all_candidates[can_ID] for can_ID in can_IDs}
                
                #make data frame of answers                        
                df = extract_answers_from_candidates(all_jobs[ja_ID], job_candidates)
                
                #check if the current date is the deadline date
                #and check if there is anything n the df
                if today.date()>=deadline and len(df)>0:
                    print('deadline passed for ja_ID: '+ja_ID+', processing ranking')
                    
                    df_ranked = rank_candidates(copy.deepcopy(df))
                    
                    #combine the df_ranked with the df
                    df_ranked = pd.concat([df_ranked['score'], df], axis=1, sort=False)
                    
                    #save dataframe locally
                    df_ranked.to_csv('ranking.csv')
                    
                    #turn the ranking data to a string
                    ranking = rank_2_text(df_ranked)
                    update_job[cfg.RANKING] = ranking
                    #send the results to zoho
                    if ranking:
                        print('Sending ranking!')
                        update_jobs[ja_ID] = update_job

                        #send email to recruiter
                        send_results_to_recruiter(contact_info,'ranking.csv')
                        
                elif deadline-today.date()<=datetime.timedelta(2) and deadline-today.date()>=datetime.timedelta(0) and deadline!=today.date():#send a reminder 2 days before the deadline
                    print('checking if people have filled in the questionnaires')
                    
                    #get the ones that have not yet received a reminder
                    can_no_ques = [can_ID for can_ID in can_IDs if can_ID not in df.index]

                    #generate reminder emails
                    all_can_no_ques = { your_key: job_candidates[your_key] for your_key in can_no_ques }
                    formal = all_clients[all_jobs[ja_ID][cfg.CLIENTID]][cfg.FORMAL]
                    compile_reminder(formal,all_jobs[ja_ID],contact_info,all_can_no_ques,'app.searchtalent.de/questionnaire?')
                    
    while True:
        try:
            #update all job openings
            update_multiple_records(cfg.JOBOPENING, update_jobs)
            print("Finish updating remaining records!")
        except:
            continue
        break                        


#    threading.Timer(wait_time, main).start()

#
#if __name__ == '__main__':
#    main()
