# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 14:27:35 2018

@author: Dr. Jim Sellmeijer
"""

#!flask/bin/python
import sys
import json
import socket
import find_profiles
from flask import Flask, request
from utils.email_compiler import generate_email, send_sms 
from utils.config import SOCKET_HOST, SOCKET_PORT, SOCK_MES_END
from flask_mail import Mail, Message


send_address = 'robot.recruiter@searchtalent.com'
email_password = 'PaulTheGreat2018!'

app = Flask(__name__)
app.config.update(
	DEBUG=True,
	#EMAIL SETTINGS
	MAIL_SERVER="smtp.ionos.com",
	MAIL_PORT=587,
	MAIL_USERNAME = send_address,
	MAIL_PASSWORD = email_password
	)
#app.app_context().push()

mail = Mail(app)

@app.route('/')
def index():
    return "Hello, Belgium!"

#job_title,city,skills,no_people,ID
@app.route('/profiles/v1', methods=['GET'])
def collect_profiles():
    '''api example:
        http://localhost:4560/profiles/v1?jobtitle=frontend%20developer&city=berlin&skills=python,css&nopeople=10&jobid=0009&socket=true'''
    print('This is standard output', file=sys.stdout)
    job_title = request.args.get('jobtitle')
    city = request.args.get('city')
    skills = request.args.get('skills')
    no_people = request.args.get('nopeople')
    job_ID = request.args.get('jobid')
    sock = request.args.get('socket')
    
    if sock and sock.lower()=='true':
        sock=True
        json_object = json.dumps({"jobid":job_ID,"message":"sending data over socket","type":"collect_profiles"})

    else:
        sock = False
        json_object = json.dumps({"jobid":job_ID,"message":"not sending data over socket","type":"collect_profiles"})
    
#    t = threading.Thread(target=find_profiles.main, args=(job_title,city,skills,int(no_people),job_ID,sock))
#    t.start()
    #find profiles
    df, items_json = find_profiles.main(job_title,city,skills,int(no_people),job_ID,sock)
    
    return json_object#json_object
    

@app.route('/message/v1',methods = ['GET'])
def compile_message():
    '''api example:
        http://localhost:4560/message/v1?canid=007&candidate=[candidate]&client=[client]&job=[job]&link=www.google.com&mode=email

        example candidate profile:
        {"CANDIDATEID": "12244000000168145", "First Name": "Dolly", "Last Name": "Udy", "City": "Halle", 
        "Country": "Belgium", "Email": "jimsell@gmail.com", "Interests": "games, food", 
        "Skill Set": "python, matlab, R, statistical analysis", "Robot_email": "true", 
        "Source": "Added by User", "prev_app": {"company": "Sony", "position": "programmer", "date": null}, 
        "time": 16, "language": "en"}
        
        example client profile:
        {"CLIENTID": "12244000000144081", "Client Name": "Sony", "Interests": "fashion, music, travelling", "formal": "formal"}
    
        example job profile:
        {"JOBOPENINGID": "12244000000144090", "CLIENTID": "12244000000144081", "CONTACTID": "12244000000446041", 
        "Posting Title": "Python developer (fake)", "Job Opening Status": "In-progress", 
        "Target Date": "2018-10-18 00:00:00+00:00", "City": "Berlin", "language": "en", 
        "Hard Skills en": "Python, C++, Machine Learning, Deep Learning, CUDA", 
        "Hard Skills de": "Python, C  , Maschinelles lernen, Tiefes Lernen, CUDA", 
        "Soft Skills en": "presentation, team work, football", 
        "Soft Skills de": "Pr\\u00e4sentation, Zusammenspiel, Fu\\u00dfball", 
        "Hard Skills zh-CN": "\\u4ecb\\u7ecd\\u76f8\\u4e92\\u4f5c\\u7528\\u8db3\\u7403", 
        "Soft Skills zh-CN": "\\u4ecb\\u7ecd\\u76f8\\u4e92\\u4f5c\\u7528\\u8db3\\u7403", 
        "pdf_path": null, "formal": "formal", "Interests": "fashion, music, travelling", 
        "custom job offer text": null, "Job Description": null}'''
    
    
    button_link = None
    mode = 'email'
    
    #variables
    can_ID = request.args.get('canid')
    
    candidate_record = json.loads(request.args.get('candidate'))
    candidate_record = {key:candidate_record[key] if candidate_record[key]!='null' else {key:None}  for key in candidate_record.keys()}
    
    client_record = json.loads(request.args.get('client'))
    client_record = {key:client_record[key] if client_record[key]!='null' else {key:None}  for key in client_record.keys()}

    job_record = json.loads(request.args.get('job'))
    job_record = {key:job_record[key] if job_record[key]!='null' else {key:None}  for key in job_record.keys()}

    button_link = request.args.get('link')
    mode = request.args.get('mode')
    
    #get email and sms
    try:
        email_, body = generate_email(candidate_record,client_record, job_record, button_link, mode)
    except Exception:
        body = None
    
    json_object = json.dumps({"canid":can_ID,"body":body,"mode":mode,"type":"compile_message"})

    #send over socket
    listening_socket = set_up_socket()
    if listening_socket:    
        listening_socket.sendall(json_object.encode()+SOCK_MES_END)

    return json_object
 
@app.route('/message/v1',methods = ['POST']) 
def send_message():
    '''example api :
                http://localhost:4560/message/v1?canid=007&mode=email&subject=New job&body=yo we have a new job!&receiveaddress=jimsell@gmail.com
        
    example message:
                "Content-Type: multipart/mixed; boundary=\"===============1377692861836973656==\"\nMIME-Version: 1.0\nSubject: Open position as Python Developer (Fake) at Sony. \nTo: jimsell@gmail.com\n\n--===============1377692861836973656==\nContent-Type: text/html; charset=\"us-ascii\"\nMIME-Version: 1.0\nContent-Transfer-Encoding: 7bit\n\nThis message was automatically generated by our Searchtalent robot recruiter. Hi Dolly, How is your afternoon going? Nice to see someone else who is interested in games! We have been in contact because of an open vacancy. Although we decided to move forward with a different candidate for the Programmer position previously, your interview performance at Sony really stood out and we would like to discuss another role, we think you might be interested in. Currently, Sony is looking for a Python Developer (Fake) to join their team. I believe you could be a great fit for this position as you have python experience. Are you looking for a new position? Feel free to click the button below. For questions, you can reply to this message.\n Click here, if you're interested!\n\nBest regards,\nMr. Robot Recruiter\n--===============1377692861836973656==--\n"'''
    #variables
    can_ID = request.args.get('canid')
    receive_address = request.args.get('receiveaddress')
    subject = request.args.get('subject')
    body = request.args.get('body')
    mode = request.args.get('mode')
    
    #send sms or email
    if mode =='sms':
        send_sms(receive_address, body)
        message = 'sms was sent to: '+str(receive_address)
    elif mode =='email':
        msg = Message(subject, sender=send_address, recipients=[receive_address])
        msg.html = body
        mail.send(msg)
        message = 'email was sent to: '+str(receive_address)
    else:
        message = 'Please specify a mode by using &mode=sms or &mode=email'
    json_object = json.dumps({"canid":can_ID,"message":message,"type":"send_message"})

    #send over socket
    listening_socket = set_up_socket()
    if listening_socket:    
        listening_socket.sendall(json_object.encode()+SOCK_MES_END)
    
    return json_object

def set_up_socket():
    try:
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serversocket.connect((SOCKET_HOST, SOCKET_PORT))
        return serversocket
    except Exception as err:
        print(err)
        return None

if __name__ == '__main__':
    app.run(port=4560, debug=True, threaded=True) # 131.159.18.209:4555