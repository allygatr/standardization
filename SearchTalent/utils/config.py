import pandas as pd
import os
path = os.path.dirname(os.path.abspath(__file__))

SOCKET_HOST = "192.168.178.85"
SOCKET_PORT = 5000
SOCK_MES_END = '[end_of_message]'.encode()

RECRUITER_COMPANY = 'Searchtalent'
RECRUITER_FNAME = 'Mr. Robot' #firstname
RECRUITER_SNAME = 'Recruiter' #lastname
RECRUITER_TITLE = 'Recruiter'
RECRUITER_SIG = ""#email signature

INDEED_CLIENT_ID = "f6750f051efa17d4d70b3b704a71f218dc06b17d2bdb771b54440375addb6209"
INDEED_CLIENT_SECRET = "Yv22PytausAD8gdAbOCjtIbIQ15NnhtaAFxA2x9ct5KrxazU33s9jbrp29JEQ2fu"
INDEED_REQUEST = "https://auth.indeed.com/resumes"


API_TOKEN = "fdb7931357e70a90c410727bc1cc39fa"
GOOGLE_API_TOKEN = 'AIzaSyCJDWTzNqW3e8ExenxLbqfIIrJOxSEznHI'
EMAIL_CHECK_TOKEN = 'XifzwXI0nVYg8ooyz8wWtr6AGfL9JAXjSQTyOVb2z4gra95xj7'
GENERAL_GET_REQUEST = "https://recruit.zoho.eu/recruit/private/json/{}/{}?authtoken={}&scope=recruitapi&fromIndex={}&toIndex={}&version=2"
SPECIFIC_GET_REQUEST = "https://recruit.zoho.eu/recruit/private/json/{}/{}?authtoken={}&scope=recruitapi&fromIndex={}&toIndex={}&version=2&id={}"
POST_REQUEST = "https://recruit.zoho.eu/recruit/private/xml/{}/updateRecords?newFormat=1?authtoken={}&scope=recruitapi&version=4&xmlData={}"

OUR_NUMBER = '+4915735996135'  # our twilio number
WHATSAPP_NUMBER = '+14155238886'  # our whatsapp number (hosted by twilio)
TWILIO_SID = 'AC71398ddf097dcd8b030e74512419a782'  # our account SID
TWILIO_API_TOKEN = 'a93b04a8276310b3756202a6ecd20f06'  # our auth token
GITHUB_API_TOKEN = '983cc0a7dd640a3926d5279502204ff1e2fb450b'
AMOUNT = 'amount'
MAX_NUM_RECORD = 200
TRIAL = 10
CANDIDATES = "Candidates"
CANDIDATEID = "CANDIDATEID"
CLIENTS = "Clients"
CLIENTID = "CLIENTID"
JOBOPENING = "JobOpenings"
JOBOPENINGID = "JOBOPENINGID"
JOBOPENINGSTATUS = 'Job Opening Status'
JOBDESCRIPTION = 'Job Description'
INTERVIEWID = 'INTERVIEWID'
NOASSCIATED = 'No of Candidates Associated'
CONTACTS = "Contacts"
CONTACTID = "CONTACTID"
INTERVIEW = "Interviews"
ATTACHMENTS = "Attachments"
GET_RECORDS = "getRecords"
GET_ASSOCIATED_CANDIDATES = "getAssociatedCandidates"
GET_RELATED_RECORDS = "getRelatedRecords"
DOWNLOAD_FILE = "downloadFile"
INTERESTS = "Interests"
FORMAL = "formal"
PERSONAL = "personal"
IMPERSONAL = "impersonal"
POSTING_TITLE = "Posting Title"
LANGUAGE = "language"
LANGUAGES = "Languages"
LANGUAGE_CODES = ["en", "de", "zh-CN"]
DEFAULT_LANGUAGE = "en"
FORMAL_LANGUAGES = ["de"]
COMPOSITE_LANGUAGES = ["en"]
EMAIL_SIGNATURE = "Email Signature"
JOB_SUMMARY = "Job Summary"
ID = "Id"
CATEGORY = "Category"
EMAIL = "Email"
LAST_EMAIL = "Last Email"
EMAIL_REMINDER = 'Email Reminder'
REMINDER_TIME = 'Reminder Time'
BUTTON = "https://app.searchtalent.de/questionnaire?{}/{}"
QUESTIONNAIRE_RESULTS = 'Questionnaire Results'
CLICKED_BUTTON = "Clicked Button"
CONTACTED_JOB = "Contacted for JobOpening"
SKILL_SET = "Skill Set"
PREVIOUS_APP = "prev_app"
COMPANY = "company"
POSITION = "position"
DATE = "date"
CLIENT_NAME = "Client Name"
FIRST_NAME = "First Name"
LAST_NAME = "Last Name"
ROBOT_EMAIL = "Robot_email"
CITY = "City"
COUNTRY = 'Country'
DEFAULT_CITY = "Berlin"
TIME = "time"
SOURCE = "Source"
JOB_TITLE = "Job Title"
PDF_PATH = "pdf_path"
SUBJECT = "Subject"
LINK = 'link'
FIRST_NAME = 'First Name'
LAST_NAME = 'Last Name'
WANTS = 'Wants'
WORK_EXPERIENCE = 'Work Experience'
LANGUAGES = 'Languages'
EVENTS = 'Events'
GROUPS = 'Groups'
EDUCATION = 'Education'
PHONE = 'Phone'
RANKING = 'Ranking'
DATABASE = 'pauldata'
DB_USER = 'paul'
DB_PASSWORD = 'Pizza2018'
DB_HOST = 'data.cjiaesqxgwr8.us-east-2.rds.amazonaws.com'
DB_PORT = '5432'
START_DATETIME = 'Start DateTime'
END_DATETIME = 'End DateTime'
TARGET_DATE = 'Target Date'
HARD_SKILLS_EN = 'Hard Skills en'
SOFT_SKILLS_EN = 'Soft Skills en'
HARD_SKILLS_DE = 'Hard Skills de'
SOFT_SKILLS_DE = 'Soft Skills de'
HARD_SKILLS_ZH_CN = 'Hard Skills zh-CN'
SOFT_SKILLS_ZH_CN = 'Soft Skills zh-CN'
CUSTOM_TEXT = 'custom job offer text'
FILE = 'file'
EXPERIENCE_TEXT = {}
EXPERIENCE_TEXT["en"] = "experience"
EXPERIENCE_TEXT["de"] = "Erfahrung"
EXPERIENCE_TEXT['zh-CN'] = "经验"
DATE_FN = ['startDate','endDate']

SEARCHQUERY = 'search query'
FORBIDDEN_WORDS = ['sales','info','enquiries','admin','mail','office','head','headteacher','reception','enquiry','marketing','post','contact','email','accounts','london','general','postmaster','enquires','design','support','mailbox','law','service','reservations','information','schooladmin','secretary','pr','enq','advice','webmaster','studio','bristol','headoffice','bookings','help','jobs','manager','property','helpdesk','clerks','bursar','recruit','manchester','enquries','postbox','contactus','administrator','editor','enquire','all','recruitment','insurance','md','hq','schooloffice','services','customerservice','birmingham','print','hire','headmaster','architects','admissions','events','it','solicitors','lawyers','uk','training','lettings','info.uk','Director','adminoffice','production','business','contracts','finance','orders','news','solutions','customerservices','partners','hello','cardiff','leeds','school','team','ask','accountants','consult','operations','holidays','inquiries','hotel','edinburgh','editorial','commercial','nottingham','action','group','swindon','customer.services','hr','insure','norwich','care','shop','travel','feedback','uksales','legal','PRINCIPAL','engineers','traffic','mailroom','registrar','parts','ops','stay','advertising','sales.uk','glasgow','administration','conferences','clerk','central','personnel','aberdeen','hostmaster','liverpool','exeter','careers','properties','ukinfo','purchasing','agency','architect','bradford','conference','projects','salesuk','sale','main','oxford','systems','management','leicester','art','ideas','me','welcome','furniture','postroom','enqs','press','chambers','quality','export','connect','inquires','hull','dundee','inbox','plymouth','people','croydon','europe','online','midlands','staff','books','coventry','construction','e-mail','info-uk','graphics','theteam','library','invest','newcastle','technical','NOEMAIL','NOMAIL','noemailadress','leisure','surveys','tech','edit','tourism','office.admin','rental','arts','details','trust','townhall','customercare','sales-uk','delivery','NOEMAIL','NOMAIL','noemailadress','leisure','surveys','tech','edit','tourism','office.admin','rental','arts','details','trust','townhall','customercare','sales-uk','delivery','townclerk','kontakt','bicester','abingdon','commerciale','amministrazione','comercial','auctions','auction','web','technik','trade','trading','infos','occasion','helpline','chairman','surveying','planning','corporate','home','informatique','master','root','club','shipping','used','treasurer','security','sport','architecture','reservation','media','development','president','hiredesk','repairs','franchise','boss','technique','freight','Equipment','membership','estate','hospitality','infodesk','general.enquiries','server','member','uk-info','bury','institute','analysis','INFORMATICA','vets','users','generalenquiries','schoolmail','admin.office','learning','producer','farmer','officeadmin','education','supervisor','schoolinfo','contact.us','school.office','webadmin','mainoffice','academy','frontdesk','euroinfo','procurement','salesinfo','academic.administrator','assistant','theoffice','genoffice','webmail','main.office','gen.enquiries','privacy']
PROG_LANGS = [lang.lower().strip() for lang in list(pd.read_excel(path+'/prog_langs.xls')['langs'])]

INDEED2DF = {"resumeKey": "INDEED_ID",
             "firstName":FIRST_NAME,
             "lastName": LAST_NAME,
             "educations": EDUCATION,
             "workExperiences": WORK_EXPERIENCE,
             "skillsList": SKILL_SET,
             "summary": "SUMMARY",
             "city":CITY,
             COMPANY:COMPANY,
             'email_domain':'email_domain'}
PUNCTUATION = '!"#$%&\'()*+|-./:;<=>?@[\\]^_`{}~'
