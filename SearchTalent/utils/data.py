# -*- coding: utf-8 -*-
"""
    SEARCH TALENT DATA SCIENCE TEAM
"""
import requests
import json
from utils.config import GENERAL_GET_REQUEST, SPECIFIC_GET_REQUEST, API_TOKEN, MAX_NUM_RECORD, LANGUAGE_CODES, TRIAL, JOBOPENINGID, \
    JOBOPENING, GET_ASSOCIATED_CANDIDATES, CANDIDATEID, COMPANY, DATE, CLIENT_NAME, POSTING_TITLE, POSITION, CITY,NOASSCIATED, \
    DATABASE,DB_USER, DB_PASSWORD,DB_HOST,DB_PORT, CLIENTID, CONTACTID, FIRST_NAME, LAST_NAME, EMAIL,INTERVIEWID,\
    JOBOPENINGSTATUS,TARGET_DATE,LANGUAGE,JOBDESCRIPTION,HARD_SKILLS_EN,HARD_SKILLS_DE,SOFT_SKILLS_EN,SOFT_SKILLS_DE,\
    HARD_SKILLS_ZH_CN,SOFT_SKILLS_ZH_CN,CUSTOM_TEXT,COUNTRY,INTERESTS,SKILL_SET,QUESTIONNAIRE_RESULTS,CLICKED_BUTTON,\
    EMAIL_REMINDER,LAST_EMAIL,ROBOT_EMAIL,SOURCE, CONTACTED_JOB,FORMAL,JOB_TITLE,EMAIL_SIGNATURE,START_DATETIME,END_DATETIME, CANDIDATES
import time
import psycopg2
import base64
import xmltodict
from math import isnan
#import pdb
import urllib3
http = urllib3.PoolManager()

import string
exclude = string.punctuation

import sys
sys.path.append('./utils/py-googletrans/')
import googletrans

translator = googletrans.Translator()

def get_attachments():
    """ query data from the attachments table """
    conn = None
    try:
        conn = psycopg2.connect(database=DATABASE, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT, sslmode='require')

        cur = conn.cursor()
        cur.execute("SELECT job_ad_id,id, category, file_name,file_data FROM attachment ORDER BY id")

        data = cur.fetchall()
        if data:
            names = [JOBOPENINGID,'id','category','file_name','file_data']
            dictionary = {d[0]:{name:d[ind] for ind,name in enumerate(names)} for d in data}
            return dictionary
        cur.close()
        conn.close()
        
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
def save_db_attachment(job_ID, path, all_attachments):
    '''turns raw file data from db into pdf'''
    if all_attachments[job_ID]['category']=='Job Summary':
        open(path, 'wb').write(base64.decodestring(all_attachments[job_ID]['file_data'].encode()))
        
def get_job_ads():
    """ query data from the job_ad table """
    conn = None
    try:
        conn = psycopg2.connect(database=DATABASE, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT, sslmode='require')

        cur = conn.cursor()
        cur.execute("SELECT id, client_id,contact_id, posting_title, job_opening_status,target_date,city, language, job_description,hard_skills_en,hard_skills_de, soft_skills_en, soft_skills_de,soft_skills_zh_cn, soft_skills_zh_cn, custom_job_offer_text FROM job_ad ORDER BY id")

        data = cur.fetchall()
        if data:
            names = [JOBOPENINGID,CLIENTID,CONTACTID,POSTING_TITLE,JOBOPENINGSTATUS,TARGET_DATE,CITY,LANGUAGE,JOBDESCRIPTION,HARD_SKILLS_EN,HARD_SKILLS_DE,SOFT_SKILLS_EN,SOFT_SKILLS_DE,HARD_SKILLS_ZH_CN,SOFT_SKILLS_ZH_CN,CUSTOM_TEXT]
            dictionary = {str(d[0]):{name:str(d[ind]) for ind,name in enumerate(names) if d[ind]!='null'} for d in data}
            return dictionary
        cur.close()
        conn.close()
        
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        

            
def get_candidates():
    """ query data from the candidate table """
    conn = None
    try:
        conn = psycopg2.connect(database=DATABASE, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT, sslmode='require')

        cur = conn.cursor()
        cur.execute("SELECT id, first_name,last_name,city,country, email,language, Interests,skill_set,questionnaire_results, clicked_button, email_reminder, last_email, robot_email, source FROM candidate ORDER BY id")

        data = cur.fetchall()
        if data:
            names = [CANDIDATEID,FIRST_NAME,LAST_NAME,CITY,COUNTRY,EMAIL,LANGUAGE,INTERESTS,SKILL_SET,QUESTIONNAIRE_RESULTS,CLICKED_BUTTON,EMAIL_REMINDER,LAST_EMAIL,ROBOT_EMAIL,SOURCE]
            dictionary = {str(d[0]):{name:str(d[ind]) for ind,name in enumerate(names) if d[ind]!='null'} for d in data}
            
#            #get associations
#            #put them in the dictionary
#            for d in ass_data:
#                if d[2]:
#                    if CONTACTED_JOB not in dictionary[str(d[0])].keys():
#                        dictionary[str(d[0])][CONTACTED_JOB] = ''
#                    dictionary[str(d[0])][CONTACTED_JOB] = dictionary[str(d[0])][CONTACTED_JOB]+','+str(d[1])

            return dictionary        

            
        cur.close()
        conn.close()
        
        

    except (Exception, psycopg2.DatabaseError) as error:
        print('get_candidates'+str(error))
        
def get_associations(all_jobs, all_candidates, get_all = False):
    """ query data from the has_job_ad table """
    conn = None
    job_ID_list = [int(job_ID) for job_ID in all_jobs.keys() if all_jobs[job_ID][JOBOPENINGSTATUS] == 'In-progress']
    try:
        conn = psycopg2.connect(database=DATABASE, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT, sslmode='require')

        cur = conn.cursor()
        cur.execute("SELECT candidate_id, job_ad_id,contacted, status FROM has_job_ad")

        data = cur.fetchall()
        if data:
#            dictionary = {d[1]:{} for d in data}
#            relevant_job_ids = list(set([d[1] for d in data]))
            #innitiate dictionary
            dictionary = {}
#            dictionary = {d[1]:[] for d in data}
            #get the job opening id
            for d in data:
                #if the job ID is in the relevant list and we have not contacted the candidate
                if d[1] in job_ID_list  and d[2]==False or get_all:
                    if str(d[1]) not in dictionary.keys():
#                        print('hoi')
                        dictionary[str(d[1])] = [str(d[0])]
#                    dictionary[d[1]] = dictionary[d[1]] +','+ str(d[0])
                    else:
                        dictionary[str(d[1])].append(str(d[0]))
                #if the job ID is in the list but we have contacted the candidate
                elif d[1] in job_ID_list and d[2]:
                    if CONTACTED_JOB not in all_candidates[str(d[0])].keys():
                        all_candidates[str(d[0])][CONTACTED_JOB] = str(d[1])
                    else:
                        all_candidates[str(d[0])][CONTACTED_JOB] = all_candidates[str(d[0])][CONTACTED_JOB]+','+str(d[1])

            return dictionary, all_candidates#data       
        
        cur.close()
        conn.close()
        
    except (Exception, psycopg2.DatabaseError) as error:
        print('get_associations '+str(error))   

def get_current_time():
    res = http.request('GET', 'http://just-the-time.appspot.com/')
    result = res.data.decode("utf-8").strip()
    return result

 
def get_clients():
    """ query data from the client table """
    conn = None
    try:
        conn = psycopg2.connect(database=DATABASE, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT, sslmode='require')

        cur = conn.cursor()
        cur.execute("SELECT id, client_name,interests,formal FROM client ORDER BY id")

        data = cur.fetchall()
        if data:
            names = [CLIENTID,CLIENT_NAME,INTERESTS,FORMAL]
            dictionary = {str(d[0]):{name:str(d[ind]) for ind,name in enumerate(names) if d[ind]!='null'} for d in data}
            return dictionary  
        cur.close()
        conn.close()
        
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        
def get_contacts(all_clients):
    """ query data from the contacts table """
    conn = None
    try:
        conn = psycopg2.connect(database=DATABASE, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT, sslmode='require')

        cur = conn.cursor()
        cur.execute("SELECT id, first_name, last_name, client_id,email, job_title, email_signature FROM contact ORDER BY id")
        
        data = cur.fetchall()

        if data:
            names = [CONTACTID,FIRST_NAME,LAST_NAME,CLIENTID,EMAIL,JOB_TITLE,EMAIL_SIGNATURE]
            dictionary = {str(d[0]):{name:str(d[ind]) for ind,name in enumerate(names) if d[ind] !='null'} for d in data}
            return dictionary  
        cur.close()
        conn.close()
        
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)        
        
def get_interviews():
    """ query data from the interviews table """
    conn = None
    try:
        conn = psycopg2.connect(database=DATABASE, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT, sslmode='require')

        cur = conn.cursor()
        cur.execute('SELECT id, client_id,candidate_id,job_ad_id,"from","to" FROM interview ORDER BY id')
#        cur.execute("SELECT 'from' FROM contact ORDER BY id")

        data = cur.fetchall()

        if data:
            names = [INTERVIEWID,CLIENTID,CANDIDATEID,JOBOPENINGID,START_DATETIME,END_DATETIME]
            dictionary = {str(d[2]):{name:str(d[ind]) for ind,name in enumerate(names) if d[ind]!='null'} for d in data}
            return dictionary  

        cur.close()
        conn.close()
        
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)    


def get_api(module, method, key):
    """
    This function is used to retrieve all records belonging to a specific module
    :param module: Name of module (Candidates, JobOpenings, Clients or Contacts)
    :param method: Name of method
    :param key: Field used as key for dictionary output (CANDIDATEID, JOBOPENINGID, CLIENTID or CONTACTID)
    :return: Dictionary of records
    """
    record_list = []
    from_index = 1
    trial = TRIAL
    while True and trial:
        try:
            response = requests.get(
                GENERAL_GET_REQUEST.format(module, method, API_TOKEN, str(from_index),
                                           str(from_index + MAX_NUM_RECORD - 1)))
            text = json.loads(response.text)
            records = text['response']['result'][module]['row']
            record_list.extend(records)
            if len(records) < MAX_NUM_RECORD:
                break
            else:
                from_index += MAX_NUM_RECORD
        except:
            trial -= 1

    if len(record_list) == 0:
        return {}

    output = {}
    for record in record_list:
        fields = {field["val"]: field["content"] for field in record['FL']}
        output[fields[key]] = fields

    return output


def filter_jobs(all_jobs, checking_field="Job Opening Status", valid_value="In-progress"):
    """
    :param all_jobs: Dictionary of all jobs
    :param checking_field: The field indicating jobs should be processed or skipped
    :param valid_value: The value indicating jobs should be processed
    :return: A list of job IDs
    """
    return [key for key, value in all_jobs.items() if NOASSCIATED in value.keys() and value[checking_field] == valid_value and int(value[NOASSCIATED])>0]


def get_associated_candidates(job_ID):
    """
    Given a job ID, retrieve list of associated candidates
    :param job_ID: Job ID
    :return: List of candidate IDs associated with job ID
    """
#    print(job_ID)
    record_list = []
    from_index = 1
    while True:
        response = requests.get(
            SPECIFIC_GET_REQUEST.format(JOBOPENING, GET_ASSOCIATED_CANDIDATES, API_TOKEN, str(from_index),
                                        str(from_index + MAX_NUM_RECORD - 1), job_ID))
        text = json.loads(response.text)
        if "'code': '4422'" in str(text):
            break
        records = text['response']['result'][JOBOPENING]['row']
        if type(records)==list:
            record_list.extend(records)
        elif type(records)==dict:
            record_list.append(records)
            
        if len(records) < 200:
            break
        else:
            from_index += MAX_NUM_RECORD

    if len(record_list) == 0:
        return []

    IDs = []
    for idx, record in enumerate(record_list):
        for field in record['FL']:
            if field['val'] == CANDIDATEID:
                IDs.append(field['content'])
    return IDs


def get_results(input, criteria):
    output = []
    if not isinstance(input, list):
        input = [input]
    for row in input:
        out = {}
        for record in row["FL"]:
            if record["val"] in criteria:
                out[record["val"]] = record["content"]
        output.append(out)
    return output


def get_attachment(job_ID, path):
    parameters = {'authtoken': API_TOKEN, 'scope': 'recruitapi'}
    parameters["id"] = job_ID
    parameters["parentModule"] = JOBOPENING
    response = requests.get('https://recruit.zoho.eu/recruit/private/json/Attachments/getRelatedRecords',
                            params=parameters)

    data = response.json()["response"]["result"]["Attachments"]["row"]
    attachments = get_results(data, ["id", "Category"])
    del parameters["parentModule"]
    parameters["version"] = 2
    for att in attachments:
        if att["Category"] == "Job Summary":
            parameters["id"] = att["id"]
            response = requests.get('https://recruit.zoho.eu/recruit/private/json/JobOpenings/downloadFile',
                                    params=parameters)
            open(path, 'wb').write(response.content)

def associate_can_2_job(job_IDs, can_IDs):
    parameters = {'authtoken': API_TOKEN, 'scope': 'recruitapi'}
    parameters['jobIds'] = ','.join(job_IDs)
    parameters['candidateIds'] = ','.join(can_IDs)
    parameters["version"] = 2
    response = requests.post('https://recruit.zoho.eu/recruit/private/json/Candidates/associateJobOpening',
                params = parameters)
    print(response.text)

    
def update_multiple_records(module, list_,mode='update'):
    """
    :param module:
    :param list_: A dictionary containing cand record infor
    :return:
    """
#    pdb.set_trace()
    xml_header = """<{}>\n""".format(module)
    xml_footer = """</{}>""".format(module)
    xml_body = ""
    index = 1
    for key, dict_ in list_.items():
        success = False
        while success == False:
            try:
                content = "\n".join("""<FL val="{}">{}</FL>""".format(key, ''.join([char for char in value if char not in '&[]'])) for key, value in dict_.items() if value)
                xml = """<row no="{}">\n{}\n</row>\n""".format(index, content)
                xml_body += xml
                success = True
                index += 1
            except ConnectionError as e:  # This is the correct syntax
                print(e)
                time.sleep(3)
    complete_xml = xml_header + xml_body + xml_footer
    if mode =='update':
        r = requests.post(
        'https://recruit.zoho.eu/recruit/private/xml/' + module + '/updateRecords?authtoken=' + API_TOKEN + '&scope=recruitapi&version=4',data = {'xmlData':complete_xml})
    elif mode =='add':
        r = requests.post(
        'https://recruit.zoho.eu/recruit/private/xml/'+module+'/addRecords?authtoken='+API_TOKEN+'&scope=recruitapi&duplicateCheck=2&version=4',data = {'xmlData':complete_xml})

    print(r.text)
    return r.text

def make_new_job(job_title,city,skills,client):
    #upload and get the XML
    xml = update_multiple_records(JOBOPENING, {0:{POSTING_TITLE:job_title,CITY:city,HARD_SKILLS_EN:skills,CLIENT_NAME:client}},'add')
    #turn the xml to a dict
    xml_dict = xmltodict.parse(xml)
    #get the IDs from the highly nested dict
    job_ID = xml_dict['response']['result']['row']['success']['details']['FL'][0]['#text']
    
    return job_ID  

def df_none_2_str(df):
    mask = df.applymap(lambda x: x is None or type(x)==float and isnan(x))
    cols = df.columns[(mask).any()]
    for col in df[cols]:
        df.loc[mask[col], col] = ''
    return df


def clean_string(string, removed_chars=[']', '}', '{', '[', '>', '|', '<', '/', '\\']):
    return "".join([char for char in string if char not in removed_chars])


def broadcast_translation(record, field, language_codes=LANGUAGE_CODES):
    """
    :param record:
    :param field:
    :param language_codes:
    :return:
    """
    src_lang_code = None
    dest_lang_codes = []
    new_fields = []
    #check the languages present in the records
    for idx, lang_code in enumerate(language_codes):
        if "{} {}".format(field, lang_code) in record:
            src_lang_code = lang_code
        else:
            dest_lang_codes.append(lang_code)
    #when source language is not empty
    if src_lang_code != None:
        src_word_list = [word for word in record["{} {}".format(field, src_lang_code)].split(',')]
        #translate from source to destination languages
        for dest_lang_code in dest_lang_codes:
            new_field = "{} {}".format(field, dest_lang_code)
            record[new_field] = ','.join(translate(src_word_list, dest_lang_code))
            new_fields.append(new_field)

    return record, new_fields


def translate(text_list, language_code):
    """
    :param text_list: List of texts (words or sentences)
    :param language_code: Destination language code. Possible values: en, de
    :return:
    """
    translations = translator.translate(text_list, dest=language_code)
    return [transl.text for transl in translations]


def detect_language(text):
    """
    :param text:
    :return:
    """
    return translator.detect(text).lang


def get_previous_app(record):
    """
    :param record: Job records
    :return:
    """
    if record == None:
        return {COMPANY: None, POSITION: None, DATE: None}
    company = record.get(CLIENT_NAME)
    position = record.get(POSTING_TITLE)
    date = record.get("Start DateTime")
    if date != None:
        date = date.split()[0]
    return {COMPANY: company, POSITION: position, DATE: date}




if __name__ == '__main__':
    pass
