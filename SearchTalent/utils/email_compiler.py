# -*- coding: utf-8 -*-
"""
Created on Thu Jul 12 10:13:32 2018

@author: Jim Sellmeijer
"""
from __future__ import print_function
import base64
import copy
import datetime
import os
import random
import re
import smtplib
from collections import OrderedDict
from email.encoders import encode_base64
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import numpy as np
from apiclient.discovery import build
from googleapiclient import errors

import sys
sys.path.append('./utils/py-googletrans/')
import googletrans

translator = googletrans.Translator()
#from googletrans import Translator
#translator = Translator()
from httplib2 import Http
from oauth2client import file, client, tools
from weather import Weather, Unit
from bs4 import BeautifulSoup
import utils.config as cfg
import utils.email_template as tmp
import string
exclude = string.punctuation
import utils.text_2_topic as text_2_topic
from utils.topic_classification.get_word2vec import get_matching_skills
from twilio.rest import Client

# instantiate a client
client_in = Client(cfg.TWILIO_SID, cfg.TWILIO_API_TOKEN)

# get the dictionary with all the sentences
directory = os.path.dirname(os.path.realpath(__file__))
button_path = directory + "/button.png"
google_credential_path = directory + "/credentials.json"

# set weather to celcius
weather = Weather(unit=Unit.CELSIUS)

# dict with lists of interest names
interests_list = {}
interests_list["base"] = ["News", "Sports", "Games", "Food", "Music", "Finance", "Fashion", "Technology", "Motivation",
                          "Traveling", "Books", "Movies & Tv", "Electronics", "Animals", "Science", "Nature", "Health",
                          "Toys", "Cars", "Politics", "Economics", "Real estate", "Art & Design", "Charity"]
interests_list["en"] = ["media", "sports", "games", "food", "music", "finance", "fashion", "technology", "motivation",
                        "traveling", "books", "movies", "electronics", "animals", "science", "nature", "health", "toys",
                        "cars", "politics", "economics", "real estate", "art & design", "charity"]
interests_list["de"] = ["die Medien", "Sport", "Spiele", "Essen", "Musik", "Finanzen", "Mode", "Technologie",
                        "Motivation", "Reisen", "Bücher", "Filme", "Elektronik", "Tiere", "Wissenschaft", "Natur",
                        "Gesundheit", "Spielzeuge", "Autos", "Politik", "Wirtschaft", "Immobilien", "Art & Design",
                        "Wohltätigkeit"]



def case(variable, case_output, default_output):
    for case, output in case_output.items():
        if eval("{}{}".format(variable, case)):
            return output
    return default_output


def translate(list_, lang):
    translations = translator.translate(list_, dest=lang)
    return [transl.text for transl in translations]


def month_converter(language_code, index):
    months = tmp.MONTH[language_code]
    return months[index - 1]


def get_string(dictionary, key, language_code, is_formal):
    """
    :param dictionary:
    :param key: possible options: A string, a list of string, or None
    :param language_code: en or de
    :param is_formal: formal or informal
    :return:
    """
    if isinstance(key, str):
        value = dictionary[key][language_code]
    elif isinstance(key, list):
        for k in key:
            dictionary = dictionary[k]
        value = dictionary[language_code]
    else:
        value = dictionary[language_code]
    if language_code in cfg.FORMAL_LANGUAGES:
        return value[is_formal]
    else:
        return value


def generate_introduction(language_code, is_formal, first_name, city, time):
    try:
        location = weather.lookup_by_location(city)
    except Exception:
        print("location not found in amazon weather, no weather mentioned in email")
        city = None

    line1 = copy.deepcopy(tmp.INTRODUCTION_GREETING[language_code][cfg.PERSONAL])  # ["Hi","Dear","Good"]
    index = case(time, OrderedDict({"<12": 0, "<18": 1}), 2)
    time_name = tmp.INTRODUCTION_TIME_OF_DAY[language_code][index]

    if city != None:
        try:
            key = case(location.condition.text.lower(), OrderedDict(
                {"in {}".format(tmp.SUNNY_WEATHER): "sunny", "in {}".format(tmp.RAINY_WEATHER): "rainy",
                 "in {}".format(tmp.STORMY_WEATHER): "stormy"}), "other")
            if language_code in cfg.FORMAL_LANGUAGES:
                weather_string = tmp.INTRODUCTION_WEATHER[language_code][is_formal][key].replace("[city]",
                                                                                                 city).replace(
                    "[time_name]", time_name)
            else:
                weather_string = tmp.INTRODUCTION_WEATHER[language_code][key].replace("[city]", city).replace(
                    "[time_name]", time_name)
        except Exception:
            weather_string = ""
    else:
        weather_string = ""

    # good morning
    if len(line1)>2:
        line1[2] = line1[2] + " " + time_name

    section_int = random.randint(0, len(line1) - 1)
    if first_name != None:
        string = "<p>{} {},</p><p></p>".format(line1[section_int], first_name)
    else:
        string = "<p>{},</p><p></p>".format(tmp.INTRODUCTION_GREETING[language_code][cfg.IMPERSONAL])

    # if the day is monday
    key_time = "weekend" if datetime.datetime.today().weekday() == 0 else "not_weekend"
    appended_string = get_string(tmp.INTRODUCTION_TIME, key_time, language_code, is_formal)

    if key_time == "not_weekend":
        if section_int != 2:
            if weather_string:
                appended_string.append(weather_string)
        else:
            time_name = tmp.INTRODUCTION_TIME_OF_DAY[language_code][3]
        section_int = random.randint(0, len(appended_string) - 1)
        appended_string = appended_string[section_int].replace("[time_name]", time_name)
    string += appended_string

    return string


def mention_nonmatching_interests(language_code, is_formal, nonmatches, text):
    # mention the candidate"s interest when there are any
    if nonmatches:
        # get the first nonmatch
        nonmatch = nonmatches[0]
        lines0 = get_string(tmp.NON_MATCHING_INTEREST, "interest", language_code, is_formal)
        # capitalize when the language is german
        if language_code in cfg.FORMAL_LANGUAGES:
            if language_code == "de":
                split_nonmatch = nonmatch.split(" ")
                if len(split_nonmatch) == 2:
                    nonmatch = "{} {}".format(split_nonmatch[0], split_nonmatch[1].capitalize())
                else:
                    nonmatch = nonmatch.title()
        string = lines0[random.randint(0, len(lines0) - 1)].replace("[nonmatch]", nonmatch)
    else:
        string = ""

    if text != None:
        topic = text_2_topic.get_topic(text)
        if topic[0] == "#":
            key = "#topic"
        else:
            key = "topic"
        if language_code == "de":
            topic = topic.capitalize()
        lines1 = get_string(tmp.NON_MATCHING_INTEREST, key, language_code, is_formal)
        string = string + lines1[random.randint(0, len(lines1) - 1)].replace("[topic]", topic)

    return string


def generate_previously_applied_section(language_code, is_formal, date_previous_application, name_previous_application,
                                        source, company_previous_application, recruiter_position, recruiter_company):
    string = ""
    # if not all values are none
    if all(v is None for v in
           [date_previous_application, name_previous_application, company_previous_application]) == False:

        lines1 = tmp.PREVIOUS_APPLICATION["lines1"][language_code]

        string = lines1[random.randint(0, len(lines1) - 1)].replace("[recruiter_position]", recruiter_position).replace(
            "[recruiter_company]", recruiter_company)

        lines2 = get_string(tmp.PREVIOUS_APPLICATION, "lines2", language_code, is_formal)
        section_int = random.randint(0, len(lines2) - 1)

        # if the date is present, mention it
        if date_previous_application != None:
            date_previous_application = datetime.datetime.strptime(date_previous_application, "%Y-%m-%d")
            year_diff = datetime.datetime.today().year - date_previous_application.year
            month_diff = datetime.datetime.today().month != date_previous_application.month
            index = case(year_diff, OrderedDict({"==1": 0, ">1": 1}), case(month_diff, OrderedDict({"": 2}), 3))
            date_str = tmp.PREVIOUS_APPLICATION["date"][language_code][index]
            if index == 2:
                date_str = date_str.replace("[month]", month_converter(language_code, date_previous_application.month))
        else:
            date_str = tmp.PREVIOUS_APPLICATION["date"][language_code][3]  # "previously"

        # if the position name is present, mention it
        if name_previous_application != None:
            pos_str = tmp.PREVIOUS_APPLICATION["position"][language_code].replace("[position]",
                                                                                  name_previous_application)  # "for the [position] position".replace("[position]",name_previous_application)
        else:
            pos_str = ""
        # if company name is present
        if company_previous_application != None and company_previous_application != recruiter_company:
            com_str = tmp.PREVIOUS_APPLICATION["company"][language_code].replace("[company_previous_application]",
                                                                                 company_previous_application)  # " at "+company_previous_application
        else:
            com_str = ""
        # capitalize line2 when date_str i at the beginning
        line2 = lines2[section_int].replace("[date]", date_str)
        if section_int == 0:
            line2 = line2[0].upper() + line2[1:]
        # replace to build final string
        string = string + " " + line2.replace("[position]", pos_str).replace("[company]", com_str)
    else:
        if source in ["Added by User", "Import"]:
            string = get_string(tmp.PREVIOUS_APPLICATION, "no_application", language_code, is_formal)[0].replace(
                "[recruiter_position]", recruiter_position).replace("[recruiter_company]", recruiter_company)
        elif source == "Data Base":
            string = get_string(tmp.PREVIOUS_APPLICATION, "no_application", language_code, is_formal)[1].replace(
                "[recruiter_position]", recruiter_position).replace("[recruiter_company]", recruiter_company)
    return string


def matching_interests(job_ad_interests, cv_interests):
    # initiate
    matches = None
    nonmatches = None

    if job_ad_interests and cv_interests:
        job_ad_interests = job_ad_interests.lower().replace(" ", "")
        cv_interests = cv_interests.lower().replace(" ", "")

        # split the matches
        job_ad_interests = job_ad_interests.split(",")
        cv_interests = cv_interests.split(",")

        matches = [inter for inter in cv_interests if inter in job_ad_interests]
        nonmatches = [inter for inter in cv_interests if inter not in job_ad_interests]

    return matches, nonmatches


def generate_current_job_offer(language_code, is_formal, source, job_title, job_company_name, job_location,
                               recruiter_company_name, custom_text):
    if job_title == None:
        job_title = "someone"

    if job_company_name == None:
        job_company_name = "our client"

    # if added by user, we have to explain the position
    if source in ["Added by User", "Import", "Data Base"]:
        # if the company uses a recruiting company, we should mention it
        if job_company_name != recruiter_company_name:
            lines = get_string(tmp.CURRENT_JOB_OFFER, "different_company", language_code, is_formal)
        else:
            lines = get_string(tmp.CURRENT_JOB_OFFER, "same_company", language_code, is_formal)
    else:
        lines = get_string(tmp.CURRENT_JOB_OFFER, "active_applicant", language_code, is_formal)

    section_int = random.randint(0, len(lines) - 1)

    # capital letter
    if section_int > 1:
        job_company_name = job_company_name.capitalize()

    # ad job location (e.g. berlin) at the end of the sentence
    if job_location == None or lines:
        location_string = ""
    else:
        location_string = " in {}.".format(" ".join([w.capitalize() for w in job_location.split(" ")]))
    string = lines[section_int].replace("[our_client]", job_company_name).replace("[job_title]",
                                                                                  job_title) + location_string + ". "
    return string


def generate_interests_mentioning_section(language_code, is_formal, company_name, matches):
    # get the interests that are matching
    # and the non matching interests
    if matches:
        strings = get_string(tmp.MATCHING_INTEREST, None, language_code, is_formal)
        string = strings[random.randint(0, len(strings) - 1)]
        # reduce number of matches to 3 when there are too many matches
        if len(matches) > 3:
            matches = matches[:2]
        interest_section = ""
        for ind, match in enumerate(matches):
            # capitalize when german
            if language_code == "de":
                match = match.capitalize()
            if ind != len(matches) - 1 and ind != 0:
                interest_section = interest_section + ", " + match
            elif ind == 0:
                interest_section = interest_section + match
            else:
                interest_section = interest_section + " " + tmp.AND[language_code] + " " + match

        string = string.replace("[interest]", interest_section).replace("[company_name]", company_name)
    else:
        string = ""
    return string


def concatenate_matching_skills(matches, language_code):
    skill_section = ""

    for ind, match in enumerate(matches):
        #        if lang=="en":
        if ind != len(matches) - 1 and ind != 0:
            skill_section = skill_section + ", " + match
        elif ind == 0:
            skill_section = skill_section + " " + match
        else:
            skill_section = skill_section + " " + tmp.AND[language_code] + " " + match
    return skill_section


def generate_skill_mentioning_section(language_code, is_formal, cv_skills, job_ad_skills):
    # if cv_skills is none, make it empty
    if cv_skills == None:
        cv_skills = ""

    # turn string with skills into lists
    job_ad_skills_lower = [skill.strip().lower() for skill in job_ad_skills.split(",")]
    job_ad_skills = [skill.strip() for skill in job_ad_skills.split(",")]
    cv_skills = [skill.strip().lower() for skill in cv_skills.split(",")]

    # skill mentioning
    # matches_inds = [ind for ind, skill in enumerate(job_ad_skills_lower) if skill in ','.join(cv_skills)]
    # matches = list(np.asarray(job_ad_skills)[matches_inds])
    matches = get_matching_skills(job_ad_skills_lower, cv_skills)

    if len(matches) > 0:
        strings = get_string(tmp.SKILL_MENTIONING, "matching", language_code, is_formal)
    else:
        strings = get_string(tmp.SKILL_MENTIONING, "not_matching", language_code, is_formal)

    section_int = random.randint(0, len(strings) - 1)
    string = strings[section_int]

    if len(matches) > 3:
        matches = matches[:3]
    elif len(matches) == 0:
        matches = job_ad_skills[:3]

    skill_section = concatenate_matching_skills(matches, language_code)
    string = string.replace("[skill]", skill_section)

    return string


def generate_conclusion(language_code, is_formal,attachment, source, button, mode):
    if mode =='email':
        next_line = ['<p>','</p>']
    else:
        next_line = ['\n','\n']

    # define button
    if button == None:
        key = "no_button"
        button = ""
    else:
        key = "button"
    if source in ["Added by User", "Import", "Data Base"]:
        lines1 = get_string(tmp.CONCLUSION, [key, "passive"], language_code, is_formal)
    else:
        lines1 = get_string(tmp.CONCLUSION, [key, "active"], language_code, is_formal)

    lines2 = tmp.CONCLUSION["closing"]["no_weekend"][
        language_code]  # ["Kind regards,","All the best,","Best,","Best regards,","Best wishes,","Yours sincerely,"]

    if datetime.datetime.today().weekday() == 4:
        string = get_string(tmp.CONCLUSION, ["closing", "weekend"], language_code, is_formal)
        datetime.datetime.today()
    else:
        string = "\n" + lines2[random.randint(0, len(lines2) - 1)] + "\n"
    if attachment:
        string = lines1[random.randint(0, len(lines1) - 1)] + get_string(tmp.CONCLUSION, "more_information", language_code,
                                                                     is_formal) + next_line[0] + button + next_line[1] + string
    else:
        string = lines1[random.randint(0, len(lines1) - 1)] + next_line[0] + button + next_line[1] + string

                    
    string = string.replace('[button]',tmp.BUTTON[language_code][mode]).replace('[email]',tmp.EMAIL[language_code][mode])

    return string


def generate_robot_text(lang, robot):
    line = ""
    if robot == "true":
        line = tmp.ROBOT[lang]  # "This message was automatically generated by our Searchtalent robot recruiter."
    return line


def translate_skills(written_lang, final_lang, string):
    # get experience text
    text = cfg.EXPERIENCE_TEXT[written_lang]
    # add "experience" for context and translate
    skills_list = translate([w + " " + text for w in string.split(",")],
                            final_lang)  # commas sometimes do not survive the translation, ^ always survives

    # take the first word from the each entry in skills_list and remove the word "experience" from it
    skills_list = [
        w.strip().split()[0].replace(cfg.EXPERIENCE_TEXT[final_lang].lower(), "").replace(cfg.EXPERIENCE_TEXT[final_lang], "")
        for w in skills_list]

    # take the first word and remove punctuation
    skills = ", ".join(["".join(ch for ch in w if ch not in exclude) for w in skills_list if len(w) > 0])

    # remove experience
    return skills


def generate_email(candidate_record,client_record, job_record, button_link, mode = 'email'):
    job_record[cfg.POSTING_TITLE] = job_record[cfg.POSTING_TITLE].title()
    if candidate_record[cfg.PREVIOUS_APP][cfg.POSITION]:
        candidate_record[cfg.PREVIOUS_APP][cfg.POSITION] = candidate_record[cfg.PREVIOUS_APP][cfg.POSITION].title()

    # translate the skills for candidate
    if candidate_record[cfg.SKILL_SET]:
        # detect language
        written_lang = translator.detect(candidate_record[cfg.SKILL_SET]).lang

        # change written language to english when it is not in the available languages
        if written_lang not in cfg.EXPERIENCE_TEXT.keys():
            written_lang = "en"
        # when the language is different, we will translate it with the context word
        if written_lang != candidate_record[cfg.LANGUAGE]:
            candidate_record[cfg.SKILL_SET] = translate_skills(written_lang, candidate_record[cfg.LANGUAGE],
                                                               candidate_record[cfg.SKILL_SET])

    # translate interests for candidate
    if candidate_record[cfg.INTERESTS]:
        # first clean
        can_interests = [can_int.strip() for can_int in candidate_record[cfg.INTERESTS].lower().split(",")]
        # get indices
        indices = [ind for ind, interest in enumerate(interests_list["base"]) if interest.lower() in can_interests]
        # translate
        candidate_record[cfg.INTERESTS] = ", ".join(
            list(np.array(interests_list[candidate_record[cfg.LANGUAGE]])[indices]))

    # translate interests for jobad
    if job_record[cfg.INTERESTS]:
        # first clean
        ja_interests = [ja_int.strip() for ja_int in job_record[cfg.INTERESTS].lower().split(",")]
        # get the indices
        indices = [ind for ind, interest in enumerate(interests_list["base"]) if interest.lower() in ja_interests]
        # translate
        job_record[cfg.INTERESTS] = ", ".join(list(np.array(interests_list[candidate_record[cfg.LANGUAGE]])[indices]))

    # specifying the button text
    sentence = get_string(tmp.CLICK, None, candidate_record[cfg.LANGUAGE], job_record[cfg.FORMAL])

    # button the text in the button and making the button
    if mode =='email':
        button = '''<a href='''+button_link+''' style="display:block;text-decoration:none;text-align:center;color:#ffffff;background-color:#f29400;border-radius:10px;max-width:480px;width:440px;width:auto;border-top:1px solid #ffffff;border-right:1px solid #ffffff;border-bottom:1px solid #ffffff;border-left:1px solid #ffffff;padding-top:5px;padding-right:20px;padding-bottom:5px;padding-left:20px;font-family:Arial,'Helvetica Neue',Helvetica,sans-serif" target="_blank" data-saferedirecturl="https://www.google.com/url?q='''+button_link+'''&amp;source=gmail&amp;ust=1536307095616000&amp;usg=AFQjCNH915RiPxtSGNkba6_yaoVmLDz-0w">
          <span style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:16px;line-height:32px"><strong>'''+sentence+'''</strong></span>
        </a>'''
    else:
        button = button_link





    # "<table><tr><th><a href=http://www.google.com>Click here, if you"re interested!</a></th></tr></table>"#"<a href=""+button_link+""><br><img src="cid:image1" style="width:200px;"><br></a>"
    # get matching and non-matching interests
    matches, nonmatches = matching_interests(job_record["Interests"], candidate_record["Interests"])
    # compile the html
    sentence_dict = {
        "robot_text": generate_robot_text(candidate_record[cfg.LANGUAGE], candidate_record[cfg.ROBOT_EMAIL]),
        "introduction": generate_introduction(candidate_record[cfg.LANGUAGE], job_record[cfg.FORMAL],
                                              candidate_record[cfg.FIRST_NAME], candidate_record[cfg.CITY],
                                              candidate_record[cfg.TIME]),
        "non_matching_interests": mention_nonmatching_interests(candidate_record[cfg.LANGUAGE], job_record[cfg.FORMAL],
                                                                nonmatches, None),
        "previous_application": generate_previously_applied_section(candidate_record[cfg.LANGUAGE],
                                                                    job_record[cfg.FORMAL],
                                                                    candidate_record[cfg.PREVIOUS_APP][cfg.DATE],
                                                                    candidate_record[cfg.PREVIOUS_APP][cfg.POSITION],
                                                                    candidate_record[cfg.SOURCE],
                                                                    candidate_record[cfg.PREVIOUS_APP][cfg.COMPANY],
                                                                    cfg.RECRUITER_TITLE,
                                                                    cfg.RECRUITER_COMPANY),
        "job_offer": generate_current_job_offer(candidate_record[cfg.LANGUAGE], job_record[cfg.FORMAL],
                                                candidate_record[cfg.SOURCE], job_record[cfg.POSTING_TITLE],
                                                client_record[cfg.CLIENT_NAME], job_record[cfg.CITY],
                                                cfg.RECRUITER_COMPANY,
                                                job_record["custom job offer text"]),
        "interests": generate_interests_mentioning_section(candidate_record[cfg.LANGUAGE], job_record[cfg.FORMAL],
                                                           client_record[cfg.CLIENT_NAME], matches),
        "skills": generate_skill_mentioning_section(candidate_record[cfg.LANGUAGE], job_record[cfg.FORMAL],
                                                    candidate_record[cfg.SKILL_SET],
                                                    job_record["Hard Skills {}".format(
                                                        candidate_record[cfg.LANGUAGE])]),
        "conclusion": generate_conclusion(candidate_record[cfg.LANGUAGE], job_record[cfg.FORMAL], job_record.get(cfg.FILE),
                                          candidate_record[cfg.SOURCE], button,mode),
        "first_name": cfg.RECRUITER_FNAME,
        "last_name": cfg.RECRUITER_SNAME,
        "signature": cfg.RECRUITER_SIG}
    
    if mode=='email':
        message = "<p>{robot_text}</p>{introduction}{non_matching_interests}<br>{previous_application}<p>{job_offer}{interests}{skills}<br>\
        {conclusion}<p>{first_name} {last_name}{signature}</p>".format(**sentence_dict)
    
        # build email
        email = MIMEMultipart()
        email[cfg.SUBJECT] = tmp.TITLE[candidate_record[cfg.LANGUAGE]].replace("[job_title]",
                                                                               job_record[cfg.POSTING_TITLE]).replace(
            "[company]", client_record[cfg.CLIENT_NAME].capitalize())
        email["To"] = candidate_record["Email"]
    
        # add text of the body to the email
        text = MIMEText(message, "html")
        email.attach(text)
    
        # add attachment pdf
        if job_record[cfg.PDF_PATH] != None:
            with open(job_record[cfg.PDF_PATH], "rb") as opened:
                openedfile = opened.read()
            attachedfile = MIMEApplication(openedfile, _subtype="pdf", _encoder=encode_base64)
            # define the name of the attachment (job title + company .pdf)
            attachedfile.add_header("content-disposition", "attachment",
                                    filename=job_record[cfg.POSTING_TITLE].replace(" ", "_") + "_" + client_record[
                                        cfg.CLIENT_NAME].replace(" ", "_") + ".pdf")
            email.attach(attachedfile)
    
            # add button
        fp = open(button_path, "rb")
        fp.close()
    elif mode=='sms':
        email = None
        message = "{robot_text}\n{introduction}{non_matching_interests}\n{previous_application}\n{job_offer}{interests}{skills}\n\
        {conclusion}\n{first_name} {last_name}".format(**sentence_dict)
        soup = BeautifulSoup(message)
        #strip the message from html for storing
        message = soup.get_text()
    
    return email, message


def send_email(email, moduleInfo):
    you = moduleInfo["Email"]

    EMAIL_REGEX = re.compile(r"[^@]+@[^@]+\.[^@]+")

    # check if you is a real email address
    if type(you) == str and EMAIL_REGEX.match(you):


        me = 'robot.recruiter@searchtalent.com'
        email["From"] = me
        you = moduleInfo["Email"]
        password = 'PaulTheGreat2018!'
#        session = smtplib.SMTP("smtp.1and1.com", 587)
        session = smtplib.SMTP("smtp.ionos.com", 587)
        session.login(me, password)
        try:
            session.sendmail(me, you, email.as_string())
            sent = True
        except Exception:
            print("Email address does not exist, could not send email. ")
            sent = False
        session.quit()

    else:
        print("not a real email address, email not sent")
        sent = False
    return sent


def send_google_message(email):
    """Send an email message.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User"s email address. The special value "me"
      can be used to indicate the authenticated user.
      message: Message to be sent.

    Returns:
      Sent Message.
    """
    # setting the user
    user_id = "me"

    # Setup the Gmail API
    SCOPES = "https://www.googleapis.com/auth/gmail.send"
    store = file.Storage("token.json")
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets(google_credential_path, SCOPES)
        creds = tools.run_flow(flow, store)
    service = build("gmail", "v1", http=creds.authorize(Http()))

    # decode the message to bytes
    raw = base64.urlsafe_b64encode(email.as_string().encode())
    raw = raw.decode()

    # send email
    try:
        message = (service.users().messages().send(userId=user_id, body={"raw": raw})
                   .execute())
        print("Message Id: %s" % message["id"])
        return message
    except errors.HttpError as error:
        print("An error occurred: %s" % error)



# send whatsapp
def send_whatsapp(contact_number, body):
    # send whatsapp
    try:
        message = client_in.messages.create(from_='whatsapp:' + cfg.WHATSAPP_NUMBER,
                                     body=body,
                                     to='whatsapp:' + contact_number)
        print(message.sid)
        sent = True
    except Exception:
        print("whatsapp can not be sent.")
        sent = False
    return sent

#send sms
def send_sms(contact_number, body):
    # send sms
    try:
        message = client_in.messages.create(to=contact_number,
                                     from_=cfg.OUR_NUMBER,
                                     body=body)
        print(message.sid)
        sent = True
    except Exception:
        print("SMS can not be sent.")
        sent = False
    
    return sent

def get_replies(to_number, from_number, whatsapp):
    if whatsapp:
        text = 'whatsapp:'
    else:
        text = ''
    # receive messages
    return client_in.messages.list(text + to_number, text + from_number)



#if __name__ == "__main__":
#    str = mention_nonmatching_interests("en", "formal", "sport, music, techniques", text)
#    print(str)
