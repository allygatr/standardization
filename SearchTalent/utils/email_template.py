INTRODUCTION_GREETING = {'en': {'personal': ['Hi', 'Dear', 'Good'], 'impersonal': ''},
                         'de': {'personal': ['Hallo', 'Hi', 'Guten'], 'impersonal': ''},
                         'zh-CN': {'personal': ['你好', '你好'], 'impersonal': ''}}

INTRODUCTION_TIME_OF_DAY = {'en': ['morning', 'afternoon', 'evening', 'day'], 'de': ['Morgen', 'Tag', 'Abend', 'Tag'],
                            'zh-CN': ['早晨好', '下午好', '晚上好', '您好']}

INTRODUCTION_WEATHER = {'en': {'sunny': "I hope you're enjoying the lovely weather in [city]! ",
                               'stormy': "I hope you're inside on this stormy [time_name] in [city]! ",
                               'rainy': "I hope you're inside on this rainy [time_name] in [city]! ",
                               'other': "I hope you're doing great on this lovely [time_name]. "},
                        'de': {'informal': {'sunny': 'Ich hoffe, du genießt das schöne Wetter in [city]! ',
                                            'stormy': 'Ich hoffe, du musst nicht vor die Tür an diesem stürmischen [time_name] in [city]! ',
                                            'rainy': 'Ich hoffe, du musst nicht vor die Tür an diesem regnerischen [time_name] in [city]! ',
                                            'other': 'Ich hoffe, dir geht es gut an diesem schönen [time_name]. '},
                               'formal': {'sunny': 'Ich hoffe, Sie genießen das schöne Wetter in [city]! ',
                                          'stormy': 'Ich hoffe, Sie müssen nicht vor die Tür an diesem stürmischen [time_name] in [city]! ',
                                          'rainy': 'Ich hoffe, Sie müssen nicht vor die Tür an diesem regnerischen [time_name] in [city]! ',
                                          'other': 'Ich hoffe, Ihnen geht es gut an diesem schönen [time_name]. '}},
                        'zh-CN': {'sunny': "我希望您在[city]正在享受晴朗的好天气!",
                               'stormy': "我希望您在[city]在这个暴风骤雨的[time_name]呆在室内! ",
                               'rainy': "我希望您在[city]在这个下雨的[time_name]呆在室内! ",
                               'other': "我希望在这个美好的[time_name]，　您一些都好! "}}

INTRODUCTION_TIME = {"weekend": {'en': 'I hope you had a nice weekend. ',
                                 'de': {'informal': 'Ich hoffe du hattest ein schönes Wochenende. ',
                                        'formal': 'Ich hoffe Sie hatten ein schönes Wochenende. '},
                                 'zh-CN': '我希望您度过了一个美好的周末。　'},
                     "not_weekend": {
                         'en': ['I hope your [time_name] is going great. ', 'How is your [time_name] going? '],
                         'de': {'informal': ['Ich hoffe dein [time_name] läuft super. ',
                                             'Wie läuft dein [time_name] bis jetzt? '],
                                'formal': ['Ich hoffe Ihr [time_name] läuft super. ',
                                           'Wie läuft Ihr [time_name] bis jetzt? ']},
                         'zh-CN': ['我希望您的[time_name]一切都好。 ', '您[time_name]过的好吗？ ']},
                     'not_weekend_good': {'en': ['How is your day going? ', 'I hope things are going well for you. '],
                                          'de': {'informal': ['Wie läuft dein Tag? ',
                                                              'Ich hoffe alles läuft gut bei dir. '],
                                                 'formal': ['Wie läuft Ihr Tag? ',
                                                            'Ich hoffe alles läuft gut bei Ihnen .']},
                                          'zh-CN': ['今天过的好吗？', '我希望您一切都好。']}
                     }

MONTH = {
    'en': ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November',
           'December'],
    'de': ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November',
           'Dezember'],
    'zh-CN': ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']}

SUNNY_WEATHER = ['sunny', 'hot']

RAINY_WEATHER = ['mixed rain and snow', 'freezing drizzle', 'drizzle', 'freezing rain', 'mixed rain and sleet',
                 'mixed snow and sleet', 'showers', 'showers', 'snow flurries', 'hail', 'sleet', 'mixed rain and hail',
                 'scattered showers']

STORMY_WEATHER = ['tornado', 'tropical storm', 'hurricane', 'severe thunderstorms', 'thunderstorms',
                  'isolated thunderstorms', 'scattered thunderstorms', 'scattered thunderstorms', 'thundershowers',
                  'isolated thundershowers']

NON_MATCHING_INTEREST = {'interest': {
    'en': ["I saw you're into [nonmatch]. Me too! ", "Nice to see someone else who is interested in [nonmatch]! "],
    'de': {'informal': ['Mir ist aufgefallen, dass du dich für [nonmatch] interessierst. Das tue ich auch! ',
                        'Es ist schön jemanden kennenzulernen, der sich auch für [nonmatch] interessiert! '],
           'formal': ['Mir ist aufgefallen, dass Sie sich für [nonmatch] interessieren. Das tue ich auch! ',
                      'Es ist schön jemanden kennenzulernen, der sich auch für [nonmatch] interessiert! ']},
    'zh-CN': ["我看到您对[nonmatch]感兴趣， 我也是！ ",
           "很高兴看到有人也对[nonmatch]感兴趣！ "]},
    '#topic': {'en': ['By the way, I liked your post with [topic]. ', 'By the way, nice post! [topic] '],
               'de': {'informal': ['Übrigens, mir hat dein Beitrag über #[topic] gefallen. ',
                                   'Übrigens, guter Beitrag! #[topic] '],
                      'formal': ['Übrigens, mir hat Ihr Beitrag über #[topic] gefallen. ',
                                 'Übrigens, guter Beitrag! #[topic] ']},
               'zh-CN': ['对了， 我很喜欢您发的关于[topic]的帖子。 ',
                      '对了， [topic]， 真是一个好帖子！']},
    'topic': {'en': ['By the way, I liked your post mentioning [topic]. ',
                     'I just read the post you shared mentioning [topic]. Very interesting! '],
              'de': {'informal': ['Übrigens, mir hat dein Beitrag bezüglich [topic] gefallen. ',
                                  'Ich habe gerade deinen geteilten Beitrag zu [topic] gelesen. Sehr interessant! '],
                     'formal': ['Übrigens, mir hat Ihr Beitrag bezüglich [topic] gefallen. ',
                                'Ich habe gerade Ihren geteilten Beitrag zu [topic] gelesen. Sehr interessant! ']},
              'zh-CN': ['对了， 我喜欢您发的提到[topic]的帖子。', '我刚刚读了您分享的关于[topic]的帖子。 非常有趣!']}}

MATCHING_INTEREST = {'en': ['I saw that you are interested in [interest], so I think you might like working here! ',
                            'I think it could be nice for you, as [company_name] is also interested in [interest]. ',
                            'I think you would like working here, as I see you are interested in [interest]. '], 'de': {
    'informal': [
        'Ich habe gesehen, dass du dich für [interest] interessierst, demzufolge könnte dir die Arbeit hier gefallen!',
        'Ich denke, es könnte dir gefallen denn [company_name] hat auch mit [interest] zu tun. ',
        'Ich denke dir könnte es gefallen hier zu arbeiten, da ich sehe, dass du dich für [interest] interessierst. '],
    'formal': [
        'Ich habe gesehen, dass Sie sich für [interest] interessieren, demzufolge könnte Ihnen die Arbeit hier gefallen! ',
        'Ich denke, es könnte Ihnen gefallen denn [company_name] hat auch mit [interest] zu tun. ',
        'Ich denke Ihnen könnte es gefallen hier zu arbeiten, da ich sehe, dass Sie sich für [interest] interessieren. ']}}

PREVIOUS_APPLICATION = {'lines1': {'en': ['We have been in contact because of an open vacancy.',
                                          "I'm a [recruiter_position] at [recruiter_company] and we have been in contact because of an open vacancy."],
                                   'de': ['Wir waren bereits in Kontakt wegen einer offenen Stelle. ',
                                          'Ich bin [recruiter_position] bei [recruiter_company] und wir waren bereits in Kontakt wegen einer offenen Stelle. '],
                                   'zh-CN': ['因为之前的一个职位空缺， 我们曾经联系过。',
                                          "我是[recruiter_company]的[recruiter_position]， 由于之前的一个职位空缺， 我们曾经联系过。"]},
                        'lines2': {'en': [
                            "[date], you applied [position][company]." + " Since you are part of our talentpool, we would like to talk to you about another opportunity. ",
                            'Although we decided to move forward with a different candidate [position] [date], your interview performance[company] really stood out and we would like to discuss another role, we think you might be interested in. ',
                            "Although you weren't selected [position] [date], your profile really stood out and we've kept you in mind for a future opening. "],
                            'de': {'informal': [
                                'Am [date], hast du dich [position][company] beworben.' + ' Da du Teil unseres Talentpools bist, würden wir gerne mit dir über eine weitere Möglichkeit sprechen. ',
                                'Auch wenn wir uns bei der Stelle [position] [date] für einen anderen Kandidaten entschieden haben, war deine Leistung im persönlichen Gespräch[company] sehr überzeugend. Deswegen würden wir gerne mit dir über eine andere Stelle sprechen, für die du dich vielleicht interessieren könntest. ',
                                'Auch wenn bei der Stelle [position] [date] die Wahl leider nicht auf dich fiel, war dein Profil derart überzeugend, dass wir dich für künftige freie Stellen in Erinnerung behalten haben. '],
                                'formal': [
                                    'Am [date], haben Sie sich [position][company] beworben.' + ' Da Sie Teil unseres Talentpools sind, würden wir gerne mit Ihnen über eine weitere Möglichkeit sprechen. ',
                                    'Auch wenn wir uns bei der Stelle [position] [date] für einen anderen Kandidaten entschieden haben, war Ihre Leistung im persönlichen Gespräch[company] sehr überzeugend. Deswegen würden wir gerne mit Ihnen über eine andere Stelle sprechen, für die Sie sich vielleicht interessieren könnten. ',
                                    'Auch wenn bei der Stelle [position] [date] die Wahl leider nicht auf Sie fiel, war Ihr Profil derart überzeugend, dass wir Sie für künftige freie Stellen in Erinnerung behalten haben. ']},
                            'zh-CN': ["在[date]您申请了[company]的[position]。" + "我们现在有另一个职位， 由于您在我们的人才库中， 我们希望能和您聊一下。",
                                   '尽管在[date]关于[position]的职位， 我们选择了另一位候选人， 但是您在[company]面试时候的表现真的非常突出， 由于我们现在又有另一个职位空缺， 希望有机会跟您聊一下， 我们认为您可能会很感兴趣。',
                                   "尽管在[date]关于[position]的职位您没能入选， 但是您的个人资历真的非常突出，　对于新的相关职位空缺，　我们首先考虑到了您。"]},
                        'date': {'en': ['last year', 'a couple of years ago', 'last [month]', "previously"],
                                 'de': ['letztes Jahr', 'vor ein paar Jahren', 'letzten [month]', 'kürzlich'],
                                 'zh-CN': ['去年', '几年前', '去年[month]', "以前"]},
                        'position': {'en': 'for the [position] position',
                                     'de': 'für die [position] Position',
                                     'ch': '关于[position]的职位'},
                        'company': {'en': ' at [company_previous_application]',
                                    'de': ' bei [company_previous_application]',
                                    'zh-CN': ' 在[company_previous_application]'},
                        'no_application': {'en': [
                            "I am a [recruiter_position] at [recruiter_company] and I found your profile online. I hope you don't mind my message.",
                            "I am a [recruiter_position] at [recruiter_company]. I just found your profile in our data base as you have applied with us before."],
                            'de': {'informal': [
                                'Ich bin [recruiter_position] bei [recruiter_company] und bin online auf dein Profil gestoßen. Ich hoffe meine Nachricht stört dich nicht. ',
                                'Ich bin [recruiter_position] bei [recruiter_company]. Ich bin gerade auf dein Profil in unserer Datenbank gestoßen, da du dich ja bereits bei uns beworben hattest.'],
                                'formal': [
                                    'Ich bin [recruiter_position] bei [recruiter_company] und bin online auf Ihr Profil gestoßen. Ich hoffe meine Nachricht stört Sie nicht zu sehr. ',
                                    'Ich bin [recruiter_position] bei [recruiter_company]. Ich bin gerade auf Ihr Profil in unserer Datenbank gestoßen, da Sie sich ja bereits bei uns beworben hatten.']},
                            'zh-CN': [
                                "我是[recruiter_company]的[recruiter_position]，　我是在网上找到的您的个人资料。 我希望您不介意我给您发的消息。",
                                "我是[recruiter_company]的[recruiter_position]。 我刚刚在我们的数据库中找到了您的个人资料，　因为您之前申请过相关职位。"]}}

CURRENT_JOB_OFFER = {'different_company': {
    'en': ['Currently, [our_client] is looking for a [job_title] to join their team',
           'We have an opening for a [job_title] at [our_client]', '[our_client] has an opening for a [job_title]',
           '[our_client] is currently looking for a [job_title]',
           '[our_client] is currently offering a new position as [job_title]',
           '[our_client] is currently looking to hire a [job_title] and we believe your skills and expertise are great match for this role'],
    'de': {'informal': ['[our_client] ist aktuell auf der Suche nach einem [job_title] zur Verstärkung ihres Teams',
                        'Wir haben eine freie Stelle als [job_title] bei [our_client]',
                        '[our_client] hat eine freie Stelle als [job_title]',
                        '[our_client] ist gerade auf der Suche nach einem [job_title]',
                        '[our_client] bietet derzeit eine neue Stelle als [job_title]',
                        '[our_client] möchte derzeit einen [job_title] einstellen und wir glauben, dass deine Fähigkeiten und Erfahrungen super auf diese Rolle passen würden'],
           'formal': ['[our_client] ist aktuell auf der Suche nach einem [job_title] zur Verstärkung ihres Teams',
                      'Wir haben eine freie Stelle als [job_title] bei [our_client]',
                      '[our_client] hat eine freie Stelle als [job_title]',
                      '[our_client] ist gerade auf der Suche nach einem [job_title]',
                      '[our_client] bietet derzeit eine neue Stelle als [job_title]',
                      '[our_client] möchte derzeit einen [job_title] einstellen und wir glauben, dass Ihre Fähigkeiten und Erfahrungen super auf diese Rolle passen würden']},
    'zh-CN': ['目前，　[our_client]正在寻找一位[job_title]加入他们的团队。', '我们有一个在[our_client]关于[job_title]的空缺职位。',
           '[our_client]有一个关于[job_title]的空缺职位。', '[our_client]目前刚刚发布一个新的[job_title]的空缺职位。',
           '[our_client]目前正在寻找一位[job_title]，　我们认为您的技能和专业知识非常适合这一角色。']},
    'same_company': {'en': ['Currently, we are looking for a [job_title] to join our team',
                            'We have an opening for a [job_title]',
                            'We are currently looking for a [job_title]',
                            'We are currently offering a new position as [job_title]',
                            'We are currently looking to hire a [job_title] and we believe your skills and expertise are great match for this role'],
                     'de': {'informal': [
                         'Wir sind zur Zeit auf der Suche nach einem [job_title] zur Verstärkung unseres Teams',
                         'Wir haben eine freie Stelle als [job_title]',
                         'Wir sind gerade auf der Suche nach einem [job_title]',
                         'Wir bieten derzeit eine neue Position als [job_title]',
                         'Wir wollen zur Zeit einen neuen [job_title] einstellen und wir glauben, dass deine Fähigkeiten und Erfahrungen super auf diese Rolle passen würden'],
                         'formal': [
                             'Wir sind zur Zeit auf der Suche nach einem [job_title] zur Verstärkung unseres Teams',
                             'Wir haben gerade eine freie Stelle als [job_title]',
                             'Wir sind gerade auf der Suche nach einem [job_title]',
                             'Wir bieten derzeit eine neue Position als [job_title]',
                             'Wir wollen zur Zeit einen neuen [job_title] einstellen und wir glauben, dass  Ihre Fähigkeiten und Erfahrungen sehr gut auf diese Rolle passen würden']},
                     'zh-CN': ['目前， 我们正在寻找一位[job_title]加入我们的团队',
                            '我们有一个关于[job_title]的职位空缺',
                            '我们目前正在寻找一位[job_title]',
                            '我们目前提供一个新的[job_title]的职位',
                            '我们目前正在寻找一位[job_title]，　我们认为您的技能和专业知识非常适合这个角色']},
    'active_applicant': {'en': ['Thank you for applying for the [job_title] position at [our_client]'],
                         'de': {'informal': [
                             'Vielen Dank für deine Bewerbung als [job_title] bei [our_client]'],
                             'formal': [
                                 'Vielen Dank für Ihre Bewerbung als [job_title] bei [our_client]']},
                         'zh-CN': ['谢谢您申请[our_client][job_title]的职位']}}

AND = {'en': 'and', 'de': 'und', 'zh-CN': '和'}

HERE = {'en': 'here', 'de': 'hier', 'zh-CN': '这里', }

CLICK = {'en': "Click here, if you're interested!",
         'de': {'formal': 'Bei Interesse hier klicken.', 'informal': 'Bei Interesse hier klicken'},
         'zh-CN': '如果您感兴趣，　请点击这里。'}

TITLE = {'en': 'Open position as [job_title] at [company]. ', 'de': 'Freie Position als [job_title] bei [company]. ',
         'zh-CN': '在[company]作为[job_title]的空缺职位。'}

ROBOT = {'en': 'This message was automatically generated by our Searchtalent robot recruiter.',
         'de': 'Diese Nachricht wurde automatisch generiert von unserem Searchtalent Recruiter Roboter.',
         'zh-CN': '此消息由我们Searchtalent的招聘机器人自动生成。'}
BUTTON = {'en':{'email':'button', 'sms':'link'},
          'de':{'email':'Button','sms':'Link'},
          'zh-CN':{'email':'按键', 'sms':'链接'}}
EMAIL = {'en':{'email':'email', 'sms':'SMS'},
         'de':{'email':'E-Mail','sms':'SMS'},
         'zh-CN':{'email':'电子邮件', 'sms':'短信'}}

REMINDER = {
    'en': 'How are you doing? A couple of days ago I sent you an [email] with a request to apply for [position] at [company]. We haven''t received your answers to our questionnaire yet. You can find it by clicking on the [button] in the previous [email] or you can find it [here]. ',
    'de': {
        'formal': 'wie geht es Ihnen? Vor ein paar Tagen habe ich Ihnen eine [email] über die [position] Position bei [company] geschickt. Ihre Antworten auf unseren Fragebogen haben wir noch nicht erhalten. Sie finden ihn, indem Sie in der vorherigen [email] auf den [button] klicken oder Sie finden ihn [here].',
        'informal': 'wie geht es dir? Vor ein paar Tagen habe ich dir eine [email] über die [position] Position bei [company] geschickt. Deine Antworten auf unseren Fragebogen haben wir noch nicht erhalten. Du findest ihn, indem du in der vorherigen [email] auf den [button] klicke oder du findest ihn [here].'},
    'zh-CN': '您还好吗？ 几天前我给您发了一封[email]，　是关于申请[company]的[position]职位的信息。 我们尚未收到您填写的调查问卷。 您可以通过点击上一封[email]中的[button]进行填写，　或者您可以在[here]填写。'}

SKILL_MENTIONING = {'matching': {
    'en': ["So, we're looking for people with[skill] experience. Your profile seems to be a good match! ",
           "I believe you could be a great fit for this position as you have[skill] experience. ",
           "You have an interesting CV especially due to your[skill] experience, so I think you could be a great fit. ",
           'I saw that your skills in[skill] are exceptional, so you might be a good fit for this position. ',
           'I can tell from your profile that you have gained solid experience in[skill] so you might be a great fit! ',
           'We were really impressed by your[skill] experience. '],
    'de': {'informal': [
        'Wir suchen nach Fachkräften mit Erfahrung im [skill]. Dein Profil scheint sehr gut darauf zu passen! ',
        'Ich glaube du könntest super auf diese Position passen, da du Erfahrung mit [skill] hast. ',
        'Du hast einen spannenden Lebenslauf. Besonders durch deine Erfahrung mit [skill] glaube ich, dass es sehr gut passen sollte. ',
        'Ich habe gesehen, dass deine Erfahrung mit [skill] herausragend sind, deswegen solltest du sehr gut auf diese Position passen. ',
        'Durch dein Profil kann ich sehen, dass du bereits viel Erfahrung mit  [skill] gesammelt hast. Du solltest also sehr gut passen! '],
        'formal': [
            'Wir suchen nach Fachkräften mit Erfahrung im [skill]. Ihr Profil scheint sehr gut darauf zu passen! ',
            'Ich glaube Sie könnten super auf diese Position passen, da Sie Erfahrung mit [skill] haben. ',
            'Sie haben einen spannenden Lebenslauf. Besonders durch Ihre Erfahrung mit  [skill] glaube ich, dass es sehr gut passen sollte. ',
            'Ich habe gesehen, dass Ihre Erfahrung mit [skill] herausragend sind, deswegen sollten Sie sehr gut auf diese Position passen. ',
            'Durch Ihr Profil kann ich sehen, dass Sie bereits viel Erfahrung mit  [skill] gesammelt haben. Sie sollten also sehr gut passen! ']},
    'zh-CN': [
        "所以，　我们正在寻找具有[skill]经验的人。 通过查看您的个人资料，　我们认为您跟这个职位非常匹配! ",
        "因为您有[skill]经验，　我相信您可能非常适合这个职位!",
        "您的简历令我们很感兴趣，　特别是由于您的[skill]经验，　我们认为您可能非常适合这个职位。 ",
        '我看到您的[skill]技能非常出色， 所以您可能非常适合这个职位。',
        '我可以从您的个人资料中看出您已经获得了[skill]方面的丰富经验，　所以您可能非常适合这个职位！ ',
        '您的[skill]的经验让我们印象深刻。 ']},
    'not_matching': {'en': ["So, we're looking for people with[skill] experience. ",
                            "You have an interesting CV. Do you also have[skill] experience? "],
                     'de': {'informal': ['Also wir suchen Leute mit Erfahrung im [skill]. ',
                                         'Du hast einen interessanten Lebenslauf. Hast du auch schon Erfahrung mit[skill]? '],
                            'formal': ['Also wir suchen Leute mit Erfahrung im [skill]. ',
                                       'Sie haben einen interessanten Lebenslauf. Haben Sie auch schon Erfahrung mit [skill]? ']},
                     'zh-CN': ["所以，　我们正在寻找具有[skill]经验的人。",
                            "您的简历令我们很感兴趣。 您有[skill]经验吗？"]}}

CONCLUSION = {'no_button': {'passive': {'en': ["If you're interested or have any questions, please let me know.",
                                               "Please let me know if you're interested in this role and if you have any further questions, feel free to ask! ",
                                               'Please let me know if I can forward your profile. Looking forward to your reply. ',
                                               'Would you be interested in hearing more? Looking forward to your response. ',
                                               "Are you looking for a new position? If so, please let me know if you're interested! ",
                                               'Did we get your attention? If yes, let me know so we can discuss the position in more detail! If you have any further questions, please do not hesitate to ask me. '],
                                        'de': {'informal': [
                                            'Bitte melde dich bei mir, wenn du Interesse hast oder noch Fragen offen sind. ',
                                            'Lass mich bitte wissen, ob du Interesse an dieser Position hast. Sollten noch Fragen offen sein, beantworte ich sie gerne! ',
                                            'Dürfte ich dein Profil weiterleiten? Ich freue mich auf deine Antwort. ',
                                            'Habe ich dein Interesse geweckt? Ich freue mich auf deine Rückmeldung. ',
                                            'Suchst du gerade nach eine neuen Herausforderung? Falls ja, freue ich mich auf deine Antwort! ',
                                            'Haben wir dein Interesse geweckt? Falls ja, melde dich bitte bei mir, damit wir die Details zu der Position besprechen können! Alle weiteren Fragen beantworte ich natürlich auch gerne. '],
                                            'formal': [
                                                'Bitte melden Sie sich bei mir, wenn Sie Interesse haben oder noch Fragen offen sind. ',
                                                'Lassen Sie mich bitte wissen, ob Sie Interesse an dieser Position haben. Sollten noch Fragen offen sein, beantworte ich sie gerne! ',
                                                'Dürfte ich Ihr Profil weiterleiten? Ich freue mich auf Ihre Antwort. ',
                                                'Habe ich Ihr Interesse geweckt? Ich freue mich auf Ihre Rückmeldung. ',
                                                'Suchen Sie gerade nach einer neuen Herausforderung? Falls ja, freue ich mich auf Ihre Antwort! ',
                                                'Haben wir Ihr Interesse geweckt? Falls ja, melden Sie sich bitte bei mir, damit wir die Details zu der Position besprechen können! Alle weiteren Fragen beantworte ich natürlich auch gerne. ']},
                                        'zh-CN': ["如果您对此感兴趣或有任何疑问，　请告诉我。",
                                               "如果您对此职位感兴趣，　请告诉我，　如果您有任何其他问题，　请随时跟我联系。 ",
                                               '请告诉我，　我是否可以转发您的个人资料。 期待您的回复。 ',
                                               '您有兴趣了解更多吗？ 期待您的回复。',
                                               "您还在寻找新职位吗？ 如果是的话，　请告诉我，　您是否对此感兴趣！ ",
                                               '您对此职位感兴趣吗？ 如果是的话，　请告诉我们，　以便我们能够跟您进一步探讨该职位！ 如果您有任何其他问题，　请尽管问我。 ']},
                            'active': {'en': ['I will get back to you with further details in the coming week. '],
                                       'de': {'informal': [
                                           "Ich werde mich in der kommenden Woche mit weiteren Einzelheiten bei dir wenden. "],
                                           'formal': [
                                               "Ich werde mich in der kommenden Woche mit weiteren Einzelheiten bei Ihnen wenden. "]},
                                       'zh-CN': ['我会在下周回复您。 ']}},
              'button': {'passive': {'en': [
                  "If you're interested and would like more information, please click the [button] below! In case of any further questions, you can ask me directly by replying to this [email]. ",
                  "Please let me know you're interested by clicking the [button] below. Or reply to this [email] if you have any questions. ",
                  'Are you looking for a new position? Feel free to click the [button] below. For questions, you can reply to this message. ',
                  'Would you be interested in hearing more? Please, click the [button] below! Looking forward to your response. ',
                  "Did we get your attention? If yes, click on the [button] below to show that you're interested. If you have any further questions, please do not hesitate to ask me. "],
                  'de': {'informal': [
                      'Bist du interessiert und wünschst dir mehr Informationen? Dann klicke einfach unten den [button]. Wenn du noch weitere Fragen haben solltest, kannst du mich auch direkt fragen, indem du auf diese [email] antwortest. ',
                      'Lass mich bitte wissen ob du interessiert bist, indem du auf den unteren [button] klickst. Du kannst auch einfach auf diese [email] antworten, wenn du weitere Fragen hast. ',
                      'Schaust du nach einer neuen Position? Klicke einfach auf den [button] hier unten. Bei Fragen kannst du auch auf diese [email] antworten. ',
                      'Würdest du gerne mehr erfahren? Klicke einfach auf den unteren [button]! Ich freue mich auf deine Rückmeldung. ',
                      'Haben wir dein Interesse geweckt? Falls ja, klicke bitte auf den unteren [button]. Bei weiteren Fragen, schreibe mir einfach. ',
                      'Bitte klicke auf den unteren [button] um Zugang zu einer kurzen Bewertung zu erhalten. '],
                      'formal': [
                          'Sind Sie interessiert und wünschen sich mehr Informationen? Dann klicken Sie einfach unten den [button]. Wenn Sie noch weitere Fragen haben sollten, können Sie mich auch direkt fragen, indem Sie auf diese [email] antworten. ',
                          'Lassen Sie mich bitte wissen ob Sie interessiert sind, indem Sie auf den unteren [button] klicken. Sie können auch einfach auf diese [email] antworten, wenn Sie weitere Fragen haben. ',
                          'Schauen Sie gerade nach einer neuen Position? Klicken Sie einfach auf den [button] hier unten. Bei Fragen können Sie auch auf diese [email] antworten. ',
                          'Würden Sie gerne mehr erfahren? Klicken Sie einfach auf den unteren [button]! Ich freue mich auf Ihre Rückmeldung. ',
                          'Haben wir Ihr Interesse geweckt? Falls ja, klicken Sie bitte auf den unteren [button]. Bei weiteren Fragen, schreiben Sie mir einfach. ']},
                   'zh-CN': ['如果您感兴趣并想了解更多信息，　请点击下面的[button]！　如果有任何其他问题，　您可以直接回复此[email]询问我。',
                         '请点击下面的[button]告诉我您是否感兴趣。　如果您有任何疑问，　请回复此[email]。',
                         '您在寻找新职位吗？　请点击下面的[button]。　如有任何问题，　您可以回复此[email]。 ', 
                         '您有兴趣了解到更多信息吗？　请点击下面的[button]！　期待您的回复。',
                         '这些是否让您感兴趣？　如果是，　请点击下面的[button]。　如果您有任何其他问题，　请尽管问我。']},  
                  'active': {'en': ['Please click the button below to access our assessment questionnaire. '],
                             'de': {'informal': [
                                 'Bitte klicke auf den unteren [button] um Zugang zu einer kurzen Bewertung zu erhalten. '],
                                 'formal': [
                                     'Bitte klicken Sie auf den unteren [button] um Zugang zu einer kurzen Bewertung zu erhalten. ']},
                             'zh-CN': ['请点击下面的按钮访问我们的评估问卷。']}},
              'closing': {'no_weekend': {
                  'en': ['Kind regards,', 'All the best,', 'Best,', 'Best regards,', 'Best wishes,',
                         'Yours sincerely,'],
                  'de': ['Grüße,', 'Viele Grüße,', 'Beste Grüße,', 'Mit freundlichen Grüßen,'],
                  'zh-CN': ['祝好!', '祝您愉快!', '祝您一切顺利!',
                         '祝您天天开心!', '祝您开心!',
                         '祝您一切顺心!']},
                  'weekend': {'en': 'Have a great weekend,',
                              'de': {'informal': 'Ich wünsche dir ein schönes Wochenende,',
                                     'formal': 'Ich wünsche Ihnen ein schönes Wochenende,'},
                              'zh-CN': '祝您周末愉快!'}},
              'more_information': {'en': 'For more information, you can find the job application in the attachment. ',
                                   'de': {
                                       'informal': 'Weitere Informationen findest du in der Stellenausschreibung im Anhang. ',
                                       'formal': 'Weitere Informationen finden Sie in der Stellenausschreibung im Anhang. '},
                                   'zh-CN': '如果您想了解更多的信息,　请详见附件中的职位描述。'}}
