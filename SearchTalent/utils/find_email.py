# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 14:10:41 2018

@author: Jim Sellmeijer
"""
import sys
sys.path.append("..")
import difflib
import os
path = os.path.dirname(os.path.abspath(__file__))
import re
import pickle
import requests
import string
exclude = string.punctuation
import time
import socket
import numpy as np
import pandas as pd
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from bs4 import BeautifulSoup
from collections import Counter
from difflib import SequenceMatcher
from heapq import nlargest as _nlargest
from PyPDF2 import PdfFileReader
from utils.google_api import google_something
import utils.config as cfg
exclude = list(set(string.punctuation))
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
email_conv_db = pickle.load(open(path+"\\email_conv_db.pkl","rb"))
company_list = np.unique(email_conv_db['company'].tolist())
company_list_lower = pd.Series([el.lower().replace('gmbh','').replace(' ag ','') for el in company_list])
timeout = 5
socket.setdefaulttimeout(timeout)
#import pdb
def get_domain(company_name):
    """
    param company_name: string with name of company
    return: domain of said company
    """
    #get company page
    try:
        search_result = google_something('email '+company_name,'',cfg.GOOGLE_API_TOKEN,1,'','','')
        #go through all urls and find the one that is closest to company_name
        all_domains = [sr['displayLink'].replace('www.','').lower() for sr in search_result[0]['items']]
        #get the domain names from the urls
        domain = '@'+difflib.get_close_matches(company_name.lower(),all_domains,n = 1, cutoff = 0.4)[0]
    except Exception:
        domain = None
    return domain

def get_conventions_from_db(company_name):
    """
    param company_name: string with name of company
    return list of conventions of said company, stored in our db
    """    
    #filter the company names that contain our company_name
    closest_match = company_list_lower[company_list_lower.str.contains(company_name, regex = False)]
    #when company name is short (e.g. LG), do not use diff lib but look for substrings with LG 
    if company_name and len(company_name)<5 and len(company_name.split())<2:
        closest_match = [name for name in closest_match if company_name in name.split()]        
    else:
        if len(closest_match) == 1:
            closest_match = closest_match
        elif len(closest_match)==0:
            closest_match = difflib.get_close_matches(company_name,company_list_lower,cutoff=.8)
        else:
            closest_match = difflib.get_close_matches(company_name,closest_match)
    found_name = company_list[[ind for ind,el in enumerate(company_list_lower) if el in closest_match]]
    if len(found_name)>0:
        domains = [email_conv_db.loc[email_conv_db['company'] == name]['domain'].values for name in found_name]
        found = [email_conv_db.loc[email_conv_db['domain']==domain[0]]for domain in domains]
        df_case = pd.concat(found)
        email_conventions_df = df_case.groupby('convention').sum()
        selected_conventions= [(email_conventions_df.loc[ind].name,email_conventions_df.loc[ind].values/sum(email_conventions_df[0].values)) for ind in email_conventions_df.index]
        return selected_conventions
    
def get_email_from_db(first_name, last_name, company,summary_=None):
    """
    param first_name: string with first name
    param last_name: string with last name
    param company: string with name of company
    param summary_: summary class object (see find_profiles), that keeps all data concerning the collection process
    return string with most likely email address
    """
    conventions = None
    if company:
        #get the email conventions for the company
        conventions = get_conventions_from_db(company.lower().replace('gmbh','').replace(' ag','').strip())
    if conventions:    
        #get the most likely one    
        most_likely_email = conventions[np.argmax([conv[1] for conv in conventions])][0].replace('{first_name}',first_name).replace('{last_name}',last_name).replace('{fl}',first_name[0]).replace('{ll}',last_name[0]).lower()
        if summary_:
            summary_.add_source(summary_.current_source,0,1)
            summary_.send_message()
        #check if it from the right company
        return most_likely_email

def get_overlap(s1, s2):
    """
    param s1 s2: string
    return overlap of strings
    """
    s = difflib.SequenceMatcher(None, s1, s2)
    pos_a, pos_b, size = s.find_longest_match(0, len(s1), 0, len(s2)) 
    return s1[pos_a:pos_a+size]

def get_close_matches_indexes(word, possibilities, n=3, cutoff=0.6):
    """Use SequenceMatcher to return a list of the indexes of the best 
    "good enough" matches. word is a sequence for which close matches 
    are desired (typically a string).
    possibilities is a list of sequences against which to match word
    (typically a list of strings).
    Optional arg n (default 3) is the maximum number of close matches to
    return.  n must be > 0.
    Optional arg cutoff (default 0.6) is a float in [0, 1].  Possibilities
    that don't score at least that similar to word are ignored.
    """
    if not n >  0:
        raise ValueError("n must be > 0: %r" % (n,))
    if not 0.0 <= cutoff <= 1.0:
        raise ValueError("cutoff must be in [0.0, 1.0]: %r" % (cutoff,))
    result = []
    s = SequenceMatcher()
    s.set_seq2(word)
    for idx, x in enumerate(possibilities):
        s.set_seq1(x)
        if s.real_quick_ratio() >= cutoff and \
           s.quick_ratio() >= cutoff and \
           s.ratio() >= cutoff:
            result.append((s.ratio(), idx))
    # Move the best scorers to head of list
    result = _nlargest(n, result)
    # Strip scores for the best n matches
    return [x for score, x in result]

def iterative_get_request(url):
    """
    param url: url
    return: text from page
    """
    body = []
    start = time.time()
    if url[-4:] == '.pdf':
        r = requests.get(url,headers=headers, timeout=5, verify=False, stream=True)
        with open('metadata.pdf', 'wb') as fd:
            for chunk in r.iter_content(2000):
                fd.write(chunk)
                if time.time() > (start + timeout):
                    break
        pdf = PdfFileReader(open('metadata.pdf', "rb"))
        text = ' '.join([page.extractText()for page in pdf.pages])
    else:
        r = requests.get(url,headers = headers,timeout = 5, verify = False)
        for chunk in r.iter_content(1024):
            body.append(chunk)
            if time.time() > (start + timeout):
                break
        html = b''.join(body)
        soup = BeautifulSoup(html)
        text = soup.get_text() #strip the html from html
    return text

def get_conventions_from_web(first_name, last_name, domain, company,summary_ = None):
    """
    param first_name: string with first name
    param last_name: string with last name
    param domain: string with domain of company website
    param company: string with name of company
    param summary_: summary class object (see find_profiles), that keeps all data concerning the collection process
    return string with most likely email address
    """
    print('getting email from: '+str(domain))   
    global email_conv_db  
    conv = None
    email = None        
    conv_list = []
    if first_name and last_name and domain:
        emails = []
        #google for email adresses with that domain
        answer = google_something('','', cfg.GOOGLE_API_TOKEN, 20,'','','email * * '+domain.replace('@',''))
        items  = [item for sublist in [ans['items'] for ans in answer if 'items'in ans.keys()] for item in sublist]
        #loop through the items
        for item in items:
            #leave the loop when we have 5 email conventions
            if len(conv_list)>5:
                break
            #collect the html from the page
            try:
                #facebook makes urllib take super long even when timeout is set
                if 'facebook.com' in item['link']:
                    continue
                print(item['link'])
                text = iterative_get_request(item['link'])
                print('text found')
            except Exception as err:
                print(err)
                continue
            #remove lines
            text = text.replace('\n',' ') 
            emails = list(set(re.findall('\S+'+domain, text+item['snippet']))) #get all email adresses from the text
            #turn text into a list of strings
            text_list = [t for t in text.split(' ') if len(t)>1]
            #get the "names" from the emails. e.g. jim.s@company.com -> jim.s
            names = [email.lower().split('@')[0].replace('email:','') for email in emails]
            #loop through the names
            for name in names:
                conv = ''
                #skip the email when it is a generic helpdesk email
                if name in cfg.FORBIDDEN_WORDS:
                    continue
                #the names are converted to identification list (ident)
                #the string is split by the "splitter" a punctuation character e.g. jim.s -> ['jim','s']
                ident = ''.join([char if char not in exclude else ' ' for char in name]).split()
                splitter = [char for char in name if char in exclude]
                if len(splitter)>0:
                    splitter = splitter[0]
                else:
                    splitter = ''
                #for each string in the ident we will look for matches in the text_list
                matches = [get_close_matches_indexes(ide,text_list) for ide in ident]
                #when any of matches is empty or matches is empty we skip
                if len(matches)==0 or any(len(m)==0 for m in matches):
                    continue
                #this will serve as place holder of a concept of the new "ident" for the candidate 
                can_ident = ['','']
                #check if the 2nd word comes after the first word
                if len(ident)==2:
                    #the fn_score counts how often the 'ident' comes after the first ident
                    #this would indicate that it is a first name
                    fn_score = [0,0]
                    for ind in matches[0]:
                        #if ident[1] in the word before ident[0]
                        if ident[1] in text_list[ind-1]:
                            fn_score[1] = fn_score[1]+1
                        #if ident[1] after ident[0]
                        elif ind != len(text_list)-1 and ident[1] in text_list[ind+1]:
                            fn_score[0] = fn_score[0]+1
                    #which one is the firstname?        
                    html_first_name = ident[np.argmax(fn_score)]
                    #put the candidate's first name in the right place in candidate ident
                    if html_first_name == ident[0] and len(ident[0])>1:
                        can_ident[0] = '{first_name}'#first_name
                    elif html_first_name == ident[1] and len(ident[1])>1:
                        can_ident[1] = '{first_name}'#first_name
                    elif html_first_name == ident[0] and len(ident[0])==1:
                        can_ident[0] = '{fl}'#first_name[0]
                    elif html_first_name == ident[1] and len(ident[1])==1:
                        can_ident[1] = '{fl}'#first_name[0]
                    #here we do the same for the last name
                    ln_score = [0,0]
                    for ind in matches[1]:
                        if ident[0] in text_list[ind-1]:
                            ln_score[1] = ln_score[1]+1
                        else:
                            ln_score[0] = ln_score[0]+1        
                    html_last_name = ident[np.argmax(ln_score)]
                    if html_last_name == ident[0] and len(ident[0])>1:
                        can_ident[0] = '{last_name}'#last_name
                    elif html_last_name == ident[1] and len(ident[1])>1:
                        can_ident[1] = '{last_name}'#last_name
                    elif html_last_name == ident[0] and len(ident[0])==1:
                        can_ident[0] = '{ll}'#last_name[0]
                    elif html_last_name == ident[1] and len(ident[1])==1:
                        can_ident[1] = '{ll}'#last_name[0]
                    #skip this email when the analysis was inconclusive
                    if len(can_ident[0])==0 or len(can_ident[1])==0:
                        continue
                    #build the email
                    conv = can_ident[0]+splitter+can_ident[1]+domain
                elif len(ident)==1:
                    #when there is only 1 ident, we have 1 name + a letter
                    #it can also be that the first name + last name are used
                    #but this is much harder to figure out as they are 1 long string
                    #without a splitter
                    #checks where the first letter from the ident comes from
                    fl_score = [0,0]
                    #same but last letter
                    ll_score = [0,0]
                    #the letters:
                    fl = ident[0][0]#first letter
                    ll = ident[0][-1]#last letter
                    for ind in matches[0]:
                        #if ident[1] in the word before ident[0]
                        if fl == text_list[ind-1][0]:
                            fl_score[0] = fl_score[0]+1
                        #if ident[1] after ident[0]
                        elif ind != len(text_list)-1 and fl==text_list[ind+1][0]:
                            fl_score[1] = fl_score[1]+1    
                        
                        if ll == text_list[ind-1][0]:
                            ll_score[0] = ll_score[0]+1
                        #if ident[1] after ident[0]
                        elif ind != len(text_list)-1 and ll==text_list[ind+1][0]:
                            ll_score[1] = ll_score[1]+1
                    #build the score array so that we can find the 4 cases
                    score_arr = np.asarray([fl_score,ll_score])
                    #normalize the array
                    norm_arr = score_arr / np.sqrt((np.sum(score_arr**2)))
                    #deduct the case from the array
                    case = np.argmax(norm_arr)
                    #when there is a difference between scores it must be a letter + name combination
                    if np.isnan(np.sum(norm_arr))==False:#np.sum(norm_arr)>0:
                        if case ==0:
                            conv = '{fl}{last_name}'+domain#first_name[0]+last_name+domain
                        elif case ==1:
                            conv = '{ll}{first_name}'+domain#last_name[0]+first_name+domain
                        elif case ==2:
                            conv = '{last_name}{fl}'+domain#last_name+first_name[0]+domain
                        elif case ==3:
                            conv = '{first_name}{ll}'+domain#first_name+last_name[0]+domain
                    else: #it must be just the surname or the firstname (without letter)
                        if len(matches[0])<2:
                            continue
                        #find the overlap between the strings that come before and after the word    
                        try:
                            warray = np.asarray([[text_list[ID-1],text_list[ID+1]] if ID !=0 and ID < len(text_list)-1 else '' for ID in matches[0]])
                        except Exception:
                            warray = None
                        if warray is not None and np.shape(warray)==(2,2):
                            len_1 = len(get_overlap(warray[0,0],warray[1,0]))
                            len_2 = len(get_overlap(warray[0,1],warray[1,1]))
                            #when there are more overlapping strings after the word it must be the firstname
                            if len_2>len_1:
                                conv = '{first_name}'+domain#first_name+domain
                            #if not, it must be the last name
                            else:
                                conv = '{last_name}'+domain#last_name+domain
                #add the email to the list
                if conv:
                    conv_list.append(conv)
    #after looping through items, when have a list get the email    
    if conv_list:
            email_conv_new = pd.Series(conv_list).value_counts().reset_index()
            email_conv_new.columns = ['convention',0]
            email_conv_new['company'] = [company]*len(email_conv_new)
            email_conv_new['domain'] = [domain]*len(email_conv_new)
            email_conv_new['source'] = ['web']*len(email_conv_new)
            email_conv_db = pd.concat([email_conv_new, email_conv_db],axis = 0,sort = True, ignore_index = True)
            #get the most likely address from the list
            if len(conv_list)>1:
                c = Counter(conv_list)
                conv = c.most_common(1)[0][0]
            else:                    
                conv= conv_list[0]
            email = conv.replace('{first_name}',first_name).replace('{last_name}',last_name).replace('{fl}',first_name[0]).replace('{ll}',last_name[0])
            if summary_:
                summary_.add_source(summary_.current_source,0,1)
                summary_.send_message()
            return email

def get_email_online(items_df,summary_ = None):
    """
    param items_df: dataframe of profiles without emails    
    param summary_: summary class object (see find_profiles), that keeps all data concerning the collection process
    return dataframe with email addresses taken from internet
    """
    
    #this function calls "get_email_online" for each None under email
    #it returns the found emails to the df and updates the conventions df
    global email_conv_db
    items_df[cfg.EMAIL] = items_df[[cfg.FIRST_NAME,cfg.LAST_NAME,'email_domain',cfg.EMAIL, cfg.COMPANY]].apply(lambda x: get_conventions_from_web(x[cfg.FIRST_NAME],x[cfg.LAST_NAME],x['email_domain'],x[cfg.COMPANY],summary_) if x[cfg.EMAIL]==None else x[cfg.EMAIL],axis = 1)
    pickle.dump(email_conv_db, open("email_conv_db.pkl", "wb"))    
    return items_df

def main_email_collector(items_df,summary_=None):
    """
    param items_df: dataframe of profiles without emails
    param summary_: summary class object (see find_profiles), that keeps all data concerning the collection process
    return dataframe with profiles with emails
    """
    #Remove all None s from the company and domain columns
    items_df = items_df.dropna(subset=[cfg.COMPANY, 'email_domain'])
    if items_df.empty!=False:
        #look for emails in conventions db
#        pdb.set_trace()
        items_df[cfg.EMAIL] = items_df[[cfg.FIRST_NAME, cfg.LAST_NAME, cfg.COMPANY]].apply(lambda x: get_email_from_db(x[cfg.FIRST_NAME],x[cfg.LAST_NAME],x[cfg.COMPANY],summary_),axis = 1) 
        #get emails from the internet        
        items_df = get_email_online(items_df,summary_)#this took 0:46:05.664187 for 293 email checks 
        #get how many emails we found
        items_df = items_df.dropna(subset=[cfg.EMAIL])
    return items_df
    
if __name__ =='__main__':
    new_candidates = pickle.load(open( "new_candidates.pkl", "rb" ))
    nc_sel_emails = ['']*len(new_candidates)
    for i,candidate_info in enumerate(new_candidates):
        new_emails= get_email_online(candidate_info['First Name'], candidate_info['Last Name'],candidate_info['email_domain'])    
        if new_emails:
            #get the most likely address from the list
            if len(new_emails)==1:
                candidate_info['Email']= new_emails[0]
            elif len(new_emails)>1:
                c = Counter(new_emails)
                candidate_info['Email'] = c.most_common(1)[0][0]
            candidate_info['potential emails'] = ', '.join(list(set(new_emails)))
            nc_sel_emails[i] = candidate_info['Email']
    
    
    
    
    