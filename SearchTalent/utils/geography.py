"""
    SEARCH TALENT DATA SCIENCE TEAM
"""
from geopy.geocoders import Nominatim
from timezonefinder import TimezoneFinder
import datetime
import pytz
import langdetect
import pycountry
from babel.core import get_global
from utils.config import LANGUAGE_CODES

geolocator = Nominatim()
timefinder = TimezoneFinder()
mapping = {country.name: country.alpha_2 for country in pycountry.countries}
import urllib3
http = urllib3.PoolManager()



def city_to_country(city):
    try:
        gl = geolocator.geocode(city)
        
        location = geolocator.reverse([gl.latitude, gl.longitude], language = 'en')
        return location.raw['address']['country']
    except Exception:
        return None 


def get_current_time():
    res = http.request('GET', 'http://just-the-time.appspot.com/')
    result = res.data.decode("utf-8").strip()
    return result

def get_local_time(city):
    location = geolocator.geocode(city)
    if location and isinstance(city, str):
        time_zone = timefinder.timezone_at(lng=location.longitude, lat=location.latitude)

        utc_dt = datetime.datetime.now()
        time = utc_dt.astimezone(pytz.timezone(time_zone)).time()
    else:
        time = datetime.datetime.now().time()
    return time.hour

def get_language(city):
    loc = geolocator.geocode(city)
    if loc and type(city) == str:
        # convert the name to english when it is not english
        if langdetect.detect(loc.address.split(',')[-1].strip()) != 'en':
            loc = geolocator.reverse(loc.point, language='en')

        country = loc.address.split(',')[-1].strip()
        try:
            # get country code from the country
            country_code = mapping.get(country.replace('The ', ''))
            # get language
            languages = get_global("territory_languages").get(country_code, {})
            # get the dominant language
            language = list(languages.keys())[0]
        except Exception:
            language = 'en'

        # turn languages we don't have into english
        if language not in LANGUAGE_CODES:
            language = 'en'
    else:
        language = None

    return language