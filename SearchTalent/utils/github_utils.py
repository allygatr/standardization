# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 10:00:46 2018

@author: Dr. Jim Sellmeijer
"""
import sys
sys.path.append("..")
import json
import pandas as pd
from utils import config as cfg
from utils.tools import remove_titles, do_api_call
        

def get_github_profiles(skills,city,summary_ = False):
    """
    param skills: string with skills, seperated by comma's
    param city: city name
    return: dataframe with found candidates' details
    """
    #get the languages from the skills
    languages = [skill.strip().lower() for skill in skills.split(',') if skill.strip().lower() in cfg.PROG_LANGS]
    names=[]
    
    #get the names of people that used the languages
    for language in languages:
        lang_query = '+language:'+language    
        r = do_api_call('https://api.github.com/search/users?access_token={}&q=repos:>1{}+location:{}&sort:followers&order=desc&per_page=100'.format(cfg.GITHUB_API_TOKEN,lang_query,city))
        if r.status_code==200:
            answer= json.loads(r.text)
            if len(answer['items'])!=30:
                names.extend(answer['items'])
        else:
            print('could not retrieve data, status code '+str(r.status_code))
    names = [name['login'] for name in names]
    answers = []
    for name in names:
        r=do_api_call("https://api.github.com/users/{}?access_token={}".format(name,cfg.GITHUB_API_TOKEN))        
        content = r.text        
        if r and r.status_code==200 and '"id":' in content:
            answer= json.loads(content)
            if answer['email']:
                answers.append(answer)
                if summary_:
                    summary_.add_source('github',1,1)
            else:
                if summary_:
                    summary_.add_source('github',1,0)
        elif 'API rate limit exceeded for user' in content or 'Bad credentials' in content or 'have triggered' in content:
            print('token in cooldown')
            break
        else:
            continue          
    #make the dataframe        
    items_df = pd.DataFrame(answers)    
    if items_df.empty == False:
        name_list = [remove_titles(name)  if name else None for name in list(items_df['name']) ]
        #split names into first name and last name
        items_df[[cfg.FIRST_NAME,cfg.LAST_NAME]]=pd.DataFrame([[name.split()[0],name.split()[-1]]if name else [None,None]for name in name_list],index = items_df.index)
        items_df= items_df.rename(columns={'followers': 'gh_followers',
                                           'following': 'gh_following',
                                           'public_gists': 'gh_public_gists',
                                           'public_repos': 'gh_public_repos',
                                           'email':cfg.EMAIL,
                                           'blog':'website',
                                           'location':cfg.CITY})
        return_df = items_df[['bio', 'website', 'company','Email','gh_followers', 'followers_url', 'gh_following','City','gh_public_gists', 'gh_public_repos','First Name', 'Last Name']]     
        return return_df    
    else:
        return pd.DataFrame()
if __name__ =='__main__':
    gh_df = get_github_profiles('python, c','Berlin')