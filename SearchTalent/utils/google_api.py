# -*- coding: utf-8 -*-
"""
Created on Wed Jul 18 16:46:55 2018

@author: Jim Sellmeijer
"""
import sys
sys.path.append("..")
import json
import socket
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

def google_something(search_term, country, api_key, no_pages, language, site_search, exact_terms, geo_loc='de', file_type=''):
    '''
    param search_term: string search query for google
    param country: string of country in which to search in
    param api_key: string of api key
    param no_pages: the amount of google pages we would like to search
    param language: the language in which the page should be
    param site_search: url of site in which we are to search
    param exact_terms: search query of words that must be in the result
    param geo_loc: country code of geo location
    param file_type: extension of file type
    return list of search results
    '''
    if exact_terms == '':
        exact_terms = ' '
    #you can access only 10 links per api call, there are 100 api calls in a day
    # setting up search engine
    # Build a service object for interacting with the API. Visit
    # the Google APIs Console <http://code.google.com/apis/console>
    # to get an API key for your own application.
    num = 10
    search_api = build("customsearch", "v1", developerKey=api_key)
    result_list = []
    try:
        for page in range(0, no_pages):
            if language:
                res = search_api.cse().list(q=search_term,
                                            cr=country,
                                            cx='014234737019847663933:yiwmu9ljhny',
                                            exactTerms=exact_terms,
                                            num=num,
                                            lr=language,
                                            start=page*num+1,
                                            siteSearch=site_search,
                                            gl=geo_loc,
                                            fileType=file_type).execute()
            else:
                res = search_api.cse().list(q=search_term,
                                            cr=country,
                                            cx='014234737019847663933:yiwmu9ljhny',
                                            exactTerms=exact_terms,
                                            num=num,
                                            start=page*num+1,
                                            siteSearch=site_search,
                                            gl=geo_loc,
                                            fileType=file_type).execute()
            result_list.append(res)
            #check if there is les than 10 results, then we move out of the loop
            if 'items' not in res.keys() or len(res['items']) < 10:
                break
    except HttpError as err:
        print(json.loads(err.content.decode('utf-8'))['error']['message'])
        return result_list
    except socket.error as socketerror:
        print("Error: ", socketerror)
        return result_list
    return result_list

if __name__=='__main__':
    api_key = 'AIzaSyCJDWTzNqW3e8ExenxLbqfIIrJOxSEznHI'
    answer = google_something('java developer','', api_key, 1,'','','Berlin')



