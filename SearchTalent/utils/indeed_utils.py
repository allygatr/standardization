import requests
import pandas as pd
import utils.config as cfg
from collections import OrderedDict
from utils.find_email import main_email_collector, get_domain


def get_candidate_list(query, location, start = 0):
    """
    Example https://auth.indeed.com/resumes/c81a41a6b3b13b69?client_id=f6750f051efa17d4d70b3b704a71f218dc06b17d2bdb771b54440375addb6209&v=1
    :param query: For example: java developer
    :param location: For example: london
    :param start: Start results at this result number, beginning with 0. Default is 0
    :return: nested dictionary.
    """
    query = query.lower().strip().replace(" ", "+")
    parameters = {'client_id': cfg.INDEED_CLIENT_ID, "v": 1, "q": query, "l": location, "start": start}
    response = requests.get(cfg.INDEED_REQUEST, params=parameters)
    data = response.json()
    return data

def get_candidate_infor(candidate_dict, fields):
    """
    :param candidate_dict: A dictionary containing candidate's information
    :param field_dict: A dictionary whose keys are field in Indeed, values are dataframe column's name
    :return: A list which have the same length and order as field_dict.values()
    """
    
    if 'workExperiences' in candidate_dict.keys() and len(candidate_dict['workExperiences'])>0:    
        company = candidate_dict['workExperiences'][0]['company']
        domain = get_domain(company)
    else:
        company = None
        domain = None
    
    if 'city' not in candidate_dict.keys():
        candidate_dict['city'] = None
    
    return [company if field=='company' else domain if field=='email_domain' else candidate_dict[field] for field in fields]

def get_indeed_profiles(job_title, location, summary_ = None):
    """
    param job_title: string of job title
    param location: string of location
    param summary_: summary class object (see find_profiles), that keeps all data concerning the collection process
    return dataframe with indeed profiles
    """
    indeed2df = OrderedDict(cfg.INDEED2DF)
    indeed_fields = list(indeed2df.keys())
    df_fields = list(indeed2df.values())
    df_list = []
    start_index = 0
    while True:
        candidates_list = get_candidate_list(job_title, location, start=start_index)["data"]["resumes"]
        num_resumes = len(candidates_list)
        if num_resumes == 0:
            break
        print ("Found {} resumes".format(num_resumes))
        candidates_list = [get_candidate_infor(list_, indeed_fields) for list_ in candidates_list]
        df_list.append(pd.DataFrame(candidates_list, columns=df_fields))
        start_index += 50
    if df_list:
        df = pd.concat(df_list, ignore_index=True)
        #get emails        
        #send before
        initial_len = len(df)
        if summary_:
            summary_.add_source('indeed', initial_len, 0)
        items_df = main_email_collector(df,summary_)
        #send after
        if summary_:
            summary_.add_source('indeed', 0, len(items_df))
        return items_df
    else:
        return pd.DataFrame()

