# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 16:52:18 2018

@author: Jim Sellmeijer
"""
import difflib
from utils.wikitdm3 import wikiKeywords
import nltk
import spacy
nlp = spacy.load('en')
import string
exclude = set(string.punctuation)
exclude.add('’')
exclude.add('–')
final_exclude = {"'", '+', ',', '#', '>', '}', ';', '&', ':', '$', '/', '~', '`', '{', '"', '|', '?', '<', '*', '.', '%', '=', '[', '^', '_', '@', ']', '!'}
from langdetect import detect
from nltk.corpus import stopwords
sw = stopwords.words('english')
sw = [s for s in sw if s!='the' and s!='a']
sw.extend(["i'm",'weve','hello','dear','hey','hi','ag',"we've",'gmbh','please','jan','feb','mar','apr','jun','jul','aug','sept','oct','nov','dec'])

import re
from textblob import TextBlob


def get_topic(string):
    
    #initualize important variables    
    topics = None
    topic = None
    lang = ''    
    
    #remove url:
    string = re.sub(r"https\S+", "", string)
    string = re.sub(r"http\S+", "", string)
    string = re.sub(r'[\w\.-]+@[\w\.-]+','', string)
    
    #get hashtags from the text that are not followed by only digits
    topics = [i  for i in string.split() if i.startswith("#") and str.isdigit(i[1:])==False ]
                  
    try:
        if detect(string)=='en':#and len(string.split())>5:# and len(string.split())>60: #ignoring non english and short messages
            lang = 'en'
            #remove non-ascii chars
            string = string.encode('ascii',errors='ignore').decode("utf-8")
        else:
            print('Language is not English.')
    except Exception:
        print('No features in text, skipping string.')

    #check the language and if we don't already have topics
    if len(topics)==0 and lang=='en' and len(string.split())>25:          
        
        string = '. '.join(re.split('; |, |\. |\!|\?|\n',string))#string.replace('\n','.')
        
        #remove repeated punctuation characters
        for char in list(final_exclude):
            string = re.sub(r'\{0}+'.format(char), char, string)
        
        
        #now use the topic to get the noun phrase to put in a sentence
        #    grammar = "NP: {<DT>?<JJ>*<NN>}"
        grammar = """NP: {<DT>?<JJ>*<NN.*>+}
           RELATION: {<V.*>}
                     {<DT>?<JJ>*<NN.*>+}
           ENTITY: {<NN.*>}"""
        cp = nltk.RegexpParser(grammar)
        
        result = cp.parse(nltk.tag.pos_tag(string.split()))
        #generate the noun phrases
        noun_phrases_list = [' '.join(leaf[0] for leaf in tree.leaves()) 
                              for tree in result.subtrees() 
                              if tree.label()=='NP'] 
        
        #remove the second part of a string that has a comma or period
        noun_phrases_list=[ phrase[:phrase.find('. ')] if'. ' in phrase else phrase for phrase in noun_phrases_list]
        
        #exclude strings with stopwords
        exclude_list = [phrase for phrase in noun_phrases_list if any(' '+n+' ' in ' '+''.join(ch for ch in phrase.lower() if ch not in ["'","\'",'"','.',',','!','?'])+' ' for n in sw) or any(n==phrase.lower() for n in sw)]            
        noun_phrases_list = [phrase for phrase in noun_phrases_list if phrase not in exclude_list]
        
        #when there are still some left over...
        if len(noun_phrases_list)!=0:
            #get the keywords
            kw, nS = wikiKeywords(' '.join(noun_phrases_list),10)
            nS = list(nS)
            for w in kw[:3]:
    #            best_matches = []
            
    #            noun_phrases_list_lower = [np.lower()for np in noun_phrases_list]
                indices_np = [ind for ind,np in enumerate(noun_phrases_list) if w in np.lower()]
                
                
                
                vals_np = [difflib.SequenceMatcher(None, w, noun_phrases_list[index].lower()).ratio() for index in indices_np]
                if len(vals_np)>0:
                    
                    if vals_np[0]>.5:
                        doc = nlp('I '+noun_phrases_list[indices_np[np.argmax(vals_np)]])
                    else:
                        break
                    
                    if doc[-1].pos_!='ADJ' and doc[-1].pos_!='ADP' and doc[-1].pos_!='VERB': #and doc[-1].pos_!='PROPN' 
                        topic = ''.join(ch for ch in doc.text[2:] if ch not in final_exclude)
                        
                        if topic[:2].lower()=='a ':
                            words = topic[2:].split()
                            words[-1] = TextBlob(words[-1]).words[0].pluralize() #pluralize
                            topic=' '.join(words)
                        elif topic[:3].lower()=='an ':
                            words = topic[3:].split()
                            words[-1] = TextBlob(words[-1]).words[0].pluralize() #pluralize
                            topic=' '.join(words)
                        break


                
                
    else:
        #remove the punctuation that is sometimes at the end of a hashtag
        topics = [topic[:-1]+''.join(ch for ch in topic[-1] if ch not in exclude) for topic in topics]
        if len(topics)>0:
            topic = topics[0]

    return topic

def unique(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]

if __name__=='__main__':
    strings = ['Many people believe the future is the #gigeconomy, but I wonder - does it really have to be gig vs. full-time employment? What if we found gigs at work instead? I love the idea of finding passion and purpose in different “gigs” while enjoying the stability and consistency of a job I love with a company I admire.',
              'Inspired by our dynamic professionals who are Imagining Better and living our purpose every day',
              'Pain associates both sensory and emotional aversive components, and often leads to anxiety and depression when it becomes chronic. Here, we characterized, in a mouse model, the long-term development of these sensory and aversive components as well as anxiodepressive-like consequences of neuropathic pain and determined their electrophysiological impact on the anterior cingulate cortex (ACC, cortical areas 24a/24b). We show that these symptoms of neuropathic pain evolve and recover in different time courses following nerve injury in male mice. In vivo electrophysiological recordings evidence an increased firing rate and bursting activity within the ACC when anxiodepressive-like consequences developed and this hyperactivity persists beyond the period of mechanical hypersensitivity. Whole-cell patch-clamp recordings also support ACC hyperactivity, as shown by increased excitatory postsynaptic transmission and contribution of NMDA receptors. Optogenetic inhibition of the ACC hyperactivity was sufficient to alleviate the aversive and anxiodepressive-like consequences of neuropathic pain, indicating that these consequences are underpinned by ACC hyperactivity. SIGNIFICANCE STATEMENT Chronic pain is frequently comorbid with mood disorders such as anxiety and depression. It has been shown that it is possible to model this comorbidity in animal models by taking into consideration the time factor. In this study, we aimed at determining the dynamic of different components and consequences of chronic pain, and correlated them with electrophysiological alterations. By combining electrophysiological, optogenetic and behavioral analyses in a mouse model of neuropathic pain, we show that the mechanical hypersensitivity, ongoing pain, anxiodepressive-consequences and their recoveries do not necessarily exhibit temporal synchrony during chronic pain processing, and that the hyperactivity of the anterior cingulate cortex is essential for driving the emotional impact of neuropathic pain.',
              'A stunt by a Swedish activist to stop the deportation of an Afghan refugee has been widely hailed on social media. University student Elin Ersson had booked the same flight as the Afghan and refused to sit down unless he was taken off the plane on Monday. She streamed her protest live on Facebook, showing a tense standoff with other passengers and airline crew. Reactions have been largely supportive of her action, although some people are accusing her of grandstanding. The video has since been widely shared and received almost two million views.',
              "A recording of a conversation in which President Donald Trump and his lawyer discuss a payoff over an alleged affair with a model has been broadcast by CNN. Mr Trump and Michael Cohen discuss buying the rights to former Playboy model Karen McDougal story. The audiotape was recorded in September 2016, two months before the election. The affair allegedly dates to 2006. Mr Trump's current lawyer Rudy Giuliani says no money was paid, and the tape does not show evidence of any crime.",
              "Jose Mourinho refused to answer whether he thought his Manchester United squad are strong enough to compete for the Premier League title next season. United have brought in three players for around £68m this summer and the 55-year-old told ESPN he would like to sign 'two more players'. Then asked at a news conference if he was confident of challenging, Mourinho said: I don't answer your question. The Portuguese manager stayed silent when pressed to explain why. United were runners-up to Manchester City last season, but finished 19 points behind the champions, and lost to Chelsea in the FA Cup final. The £47m spent on midfielder Fred has been their biggest outlay of the current window so far. The club have also been linked with Chelsea's Brazilian midfielder Willian, Eintracht Frankfurt's Croatia winger Ante Rebic and Tottenham's Belgian defender Toby Alderweireld. Mourinho was speaking to the media in Los Angeles before his side's pre-season friendly against AC Milan on Thursday. The United boss said he was not fully satisfied on the USA tour because he was without several members of his first-team squad, who are on a break following the World Cup.",
              'In one of my favorite books Team of Teams, General McChrystal argues that collective purpose is the only way to break down the silos that exist in today’s governmental and commercial organizations. So why do we keep allowing short term metrics to rebuild those walls? It’s time to let purpose and long-term value guide our business practices once and for all.',
              'The future of smart city initiatives is rapidly approaching - and the key to success? Public private partnerships. As a citizen, I want to see greater speed, agility and collaboration to drive tangible progress and outcomes for smarter, more resilient cities. @Alison Kay #smartcities',
              'Many of our clients’ careers and companies come with heavy restrictions on personal investment selection. We would love the opportunity to help you with your independence requirements.',
              '''The first rule of effective meetings is: Cut out the unnecessary ones. The second rule of effective meetings is: Call only the relevant people (and don't mind sitting out). That's the advice coming in from Directi's HR head Margaret Dsouza and talent acquisition head Ameya Ayachit, who feel team messaging apps can often substitute for day-to-day meetings. Call one if you must, but limit it to those who have a point to make or are important in the decision-making. "We practise that at Directi – sometimes it's just an FYI to the managers," Dsouza tells Abhigyan Chand. Here's EP1 of #WorkplaceWise. #LinkedInStudio #LinkedInVideo Cc: Pushkar Kulkarni PS: Watch the video for rules 3 through 8.''',
              '''Novichok: Victim found poison bottle in branded box The man who found the bottle of the nerve agent Novichok which killed his partner said it was in a glass bottle within an "expensive-looking" box. Charlie Rowley, who was also poisoned, said he gave his partner Dawn Sturgess the box - which he believed to be perfume - as a present. Speaking to ITV News, he said Ms Sturgess grew ill within 15 minutes of spraying the substance on her hands. Ms Sturgess, 44, died a week later on 8 July in Salisbury Hospital. Mr Rowley was discharged from the same hospital on Friday, 20 July, three weeks after being exposed to the nerve agent. Amesbury poisoning: What we know so far Police 'identify Novichok suspects' Russian spy: What happened to the Skripals? The 45-year-old believes he had the glass bottle at his home for a couple of days before giving it to his partner. He described the bottle as having a plastic dispenser which was held in a cardboard box with a plastic moulding. He said it looked expensive and that Ms Sturgess recognised the brand on the box. In an interview with ITV News, he said: "I do have a memory of her spraying it on her wrists and rubbing them together. "I guess that's how she applied it and became ill. I guess how I got in contact with it is when I put the spray part to the bottle... I ended tipping some on my hands, but I washed it off under the tap. The poisoning of Mr Rowley and Ms Sturgess came four months after the case of the former Russian spy Sergei Skripal and his daughter, Yulia. The pair were found unconscious on a park bench having come into contact with Novichok. Police are believed to have identified the suspected perpetrators of the Novichok attack on Russian former spy Mr Skripal and his daughter. Deputy Chief Constable Paul Mills of Wiltshire Police said on Tuesday that they may "never be able to tell" whether there is "anything else out there". He said that police were using "intelligence-led" methods to uncover the places visited by people who have come into contact with Novichok, and then conducting "meticulous" searches. Public Health England has advised people living in the Salisbury and Amesbury area not to pick up items such as syringes, needles, cosmetics or objects made of plastic, metal, or glass.''',
              '''This is a very special month for me. After 14 amazing years I am leaving Unilever to start an exciting journey as General Manager in Spain for Upfield (Plant Based Food Leader with Brands like Flora, Proactiv, Tulipán or Krona) I am extremely thankful to Unilever and its people for all these years and for its special purpose driven business approach. And now, a new exciting journey starts in Upfield, with an amazing team and working to make our plant based brands great! Let’s go!''',
              "How Zumba's founders turned a video on the beach into a global dance workout phenomenon:",
              '''The former chief executive of carmaker Fiat-Chrysler, Sergio Marchionne, has died in hospital in Zurich aged 66. He was replaced four days ago when his health worsened following complications from surgery on his right shoulder. Mr Marchionne, who was also Ferrari's chairman, had led the combined company for more than a decade and planned to step down next year. Mr Marchionne has been succeeded by Briton Mike Manley, head of the Italian-American firm's Jeep division. "Unfortunately, what we feared has come to pass. Sergio Marchionne, man and friend, is gone," said group chairman John Elkann, a member of the Agnelli family that controls the company. Mr Elkann praised Mr Marchionne's "values of humanity, responsibility and open-mindedness". He added: "My family and I will always be grateful for what he has done."''']     
    
    
    #get the word topics out of the text
    topics =[get_topic(string)for string in strings]