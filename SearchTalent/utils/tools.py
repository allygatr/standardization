# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 11:51:05 2018

@author: Dr. Jim Sellmeijer
"""
import unicodedata
import requests
import json
import time
import pandas as pd
from utils import config as cfg
from utils.data import get_candidates
from utils.data import df_none_2_str

def get_matches_from_zoho(skills, city, summary_):
    """
    param skills: list of skills (strings)
    param city: string of city name
    return dataframe with people from database that adhere to the search criteria (city + skills)
    """
    #get people from our db
    all_candidates = get_candidates()
    all_can_df = pd.DataFrame.from_dict(all_candidates,orient='index')
    #for each skill, check if there are people that have these skills in their skillset       
    #remove the punctuation (to remove regex characters)
    skills_regex = ''.join([char for char in skills.lower() if char not in cfg.PUNCTUATION])
    #remove single letter skills
    skills_regex = '|'.join([skill.strip() for skill in skills_regex.split(',') if len(skill.strip())>1])
    #remove the ones with unknown city
    all_can_df = all_can_df.dropna(subset=['Skill Set',cfg.CITY])
    #lower the strings
    all_can_df['Skill Set'] = all_can_df['Skill Set'].str.lower()
    all_can_df[cfg.CITY] = all_can_df[cfg.CITY].str.lower()
    #get the ones that have a skill we want and are in the city we want
    ppl_from_db = all_can_df[all_can_df['Skill Set'].str.contains(skills_regex, regex = True) & all_can_df[cfg.CITY].str.contains(city.lower(), regex = True)]    
    initial_len = len(ppl_from_db)
    if summary_:
        summary_.add_source('database',initial_len,0)
    #remove the ones without emails
    ppl_from_db = df_none_2_str(ppl_from_db.dropna(subset=[cfg.EMAIL]))
    if summary_:
        summary_.add_source('database',0,len(ppl_from_db))
    return ppl_from_db      

def extract_skills(string, skills):
    """
    param string: string with skill information in it
    param skills: list of skills that we are looking for in the string
    return: dictionary with found skills
    """
    found_skills = {skill.strip():None  for skill in skills if skill.lower().strip() in string.lower()}
    if len(found_skills)>0:
        return found_skills

def do_api_call(url):
    counts = 0
    while True:
        try:
            r = requests.get(url)
            break
        except Exception as err:
            print(err)
            counts+=1
            time.sleep(3)
        if counts>10:
            print('could not reach api')
            r = None
            break
            
    return r
def prepare_for_zoho(new_candidates):
    """
    param new_candidates: dictionary with profile data
    return: dictionary in the right shape for zoho api
    """
    #the dictionary that we get from the scraping has fields zoho doesn't have
    #so we should end up with a dictionary that has all the right info    
    fields = [cfg.FIRST_NAME,cfg.LAST_NAME,cfg.EMAIL,cfg.SKILL_SET,cfg.INTERESTS, cfg.CITY, cfg.WANTS,cfg.LANGUAGES,cfg.EVENTS,cfg.WORK_EXPERIENCE,cfg.EDUCATION,'potential emails','link',cfg.SEARCHQUERY]    
    update_dict= {ID:{ind:{field:el[field] for field in fields if field in el.keys()} for ind,el in enumerate(new_candidates[ID])} for ID in new_candidates.keys()}            
    return update_dict

def verify_email(emails):
    """
    param emails: list variations of someone's potential email address that 
    should be verified through email verifier api to see which one is the right one
    return: positive email
    """
    #verify-email.org
    for email in emails:
        r = requests.get('https://app.verify-email.org/api/v1/'+cfg.EMAIL_CHECK_TOKEN+'/verify/'+email)
        if r.status_code ==200 and json.loads(r.text)['status']==1:
            return email
            break
        elif r.status_code==402:
            print('Must pay verify-email.org to check email.')
            return ''
            break
            
def strip_accents(text):
    """
    param text: text that should be stripped from accents
    return: normalized text
    """
    return unicodedata.normalize('NFD', text).encode('ascii', 'ignore').decode("utf-8")   

def remove_site_string(x):
    """
    param x: title string from page found through google search api (google_api.google_something)
    return: cleaned string
    """
    return x.replace(' | XING','').replace('...','').replace(' | LinkedIn','')

def remove_titles(x):
    """
    param x: string
    return: string without titles
    """
    return x.lower().replace('dr.','').replace('prof.','').replace('.ing','').strip()

def prepare_df(items_df):
    """
    param items_df: dataframe constructed from google_api.google_something's search results
    return: dataframe with names, position and company split from page title
    """
    #remove xing and linkedin text
    items_df['title'] = items_df["title"].map(lambda x: remove_site_string(x))
    #split te text into new columns
    items_df = pd.concat([items_df, pd.DataFrame(items_df.title.str.split(' - ',2).tolist(),columns = ['name',cfg.POSTING_TITLE,'company'])], axis=1, join='inner')
    items_df['name'] = items_df['name'].map(lambda x: remove_titles(x))    
    name_list = items_df.name.str.split(' ').tolist()    
    items_df = pd.concat([items_df,pd.DataFrame([[name[0],name[-1]] for name in name_list],columns = [cfg.FIRST_NAME,cfg.LAST_NAME])], axis=1, join='inner')
    return items_df