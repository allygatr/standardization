"""
author : Prabhu
edited by Jim
"""
from __future__ import division
import io
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np

def load_vectors(fname):
    fin = io.open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore')
    data = {}
    for line in fin:
        tokens = line.rstrip().split(' ')
        data[tokens[0]] = [float(i) for i in tokens[1:]]
    return data

with open("utils/topic_classification/topics", 'r') as f:
    output = f.readlines()
    list_of_topics = [text[:-1].lower() for text in output]

raw_data = load_vectors('utils/topic_classification/glove.6B.50d.txt')
topics = [np.array([raw_data[k] for k in key.split()]) for key in list_of_topics]
topics = [np.mean(u, axis=0) if u.shape[0] != 1 else u[0, :] for u in topics]
topics = np.stack(topics)

def get_top_interests(cand_interest, threshold = 0.6):
    #get vector for each word in the interest sentence (for each interest in the interests list) 
    interest = [np.array([raw_data[k] for k in key.split()]) for key in cand_interest]
    #take the mean of these vectors
    interest = [np.mean(u, axis=0) if u.shape[0] != 1 else u[0, :] for u in interest]
    #put them all together through stacking
    interest = np.stack(interest)
    #get the similarity between each candidate interest and each topic
    m = cosine_similarity(topics, interest)
    #put a 0 where we are below the threshold
    m[m<threshold] = 0
    #if there are more than 0
    if np.count_nonzero(m) > 0:
        indices = [np.argmax(m[:,i]) for i in range(0,len(cand_interest))]
        result = list(np.asarray(list_of_topics)[indices])
        return result
    else:
        return []
    
def get_matching_skills(list_cand_skills, list_job_skills, threshold = 0.6):
    job_skills = [np.array([raw_data[k] for k in key.split()]) for key in list_job_skills]
    job_skills = [np.mean(u, axis=0) if u.shape[0] != 1 else u[0, :] for u in job_skills]  # row 29
    job_skills = np.stack(job_skills)
    # get vector for each word in the interest sentence (for each interest in the interests list)
    # divide the interests more than 1 word into single ones
    cand_skills = [np.array([raw_data[k] for k in key.split()]) for key in list_cand_skills]
    # take the mean of these vectors, if the interests have more than 1 word.
    cand_skills = [np.mean(u, axis=0) if u.shape[0] != 1 else u[0, :] for u in cand_skills]
    # put them all together through stacking
    cand_skills = np.stack(cand_skills)
    # get the similarity between each candidate interest and each topic
    m = cosine_similarity(job_skills, cand_skills) # 29 list, each with e.g. 3 elements if length of interest = 3
    # put a 0 where we are below the threshold
    m[m<threshold] = 0
    # if there are more than 0
    if np.count_nonzero(m) > 0:
        indices = [np.argmax(m[:,i]) for i in range(0,len(cand_skills)) if np.max(m[:,i]) != 0] # np.argmax returns the indices of the maximum values in the candidate list for each skill, if max of that column of skills is not zero.
        result = list(np.asarray(list_job_skills)[indices])
        return result
    else:
        return []

if __name__ == '__main__':
    ori_cand_interest = ['playing piano', 'music', 'jazz']
    print (get_top_interests(ori_cand_interest))
