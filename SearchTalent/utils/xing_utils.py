# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 10:44:48 2018

@author: Dr. Jim Sellmeijer
"""
import sys
sys.path.append("..")
import os
import platform
import math
import pandas as pd
from progress.bar import Bar
from selenium import webdriver
from utils import config as cfg
from utils.find_email import get_domain, main_email_collector
from utils.geography import city_to_country, get_language

from utils.tools import prepare_df,extract_skills
from utils.google_api import google_something
path = os.path.dirname(os.path.abspath(__file__))
XING = 'https://www.xing.com/profile/'
LINKEDIN = 'https://www.linkedin.com/in'
sources = ['xing','linkedin']
if platform.system()=='Windows':
    driver_file = path+'/chromedriver_windows.exe'
else:
    driver_file = path+'/chromedriver_linux'
    
def google_for_profiles(job_title, skills, city, no_people, summary_ = None):
    """
    param job_title: string of job title
    param skills: string of skills seperated by comma's
    param city: string of city
    param no_people: int of amount of people required
    param summary_: summary class object (see find_profiles), that keeps all data concerning the collection process
    return dataframe with google search results
    """
    all_items_df = pd.DataFrame()
    country = city_to_country(city)
    mand_word = city
    st_list = [w.strip() for w in skills.split(',')]
    st_list.insert(0,job_title.strip())
    if get_language(city)=='de':
        sites = [XING,LINKEDIN]
    else:
        sites = [LINKEDIN]

    
    #number of pages
    no_pages = math.ceil(no_people/10)
    #use google to search for a specific job type profile, then scrape it
    #while loop is broken when enough people are found
    url_list = []
    ind = 0
    while True:
        #first we look with all the search terms in st_list
        if ind == 0:
            search_term = ' AND '.join(st_list)
        #then we combine with the first word and the index
        else:
            search_term = ' AND '.join([st_list[0],st_list[ind]])
        print('Searching with: '+search_term)
        #search via google on XING and LinkedIn
        for site_ind,site in enumerate(sites):
            items = []
            answer = google_something(search_term,'', cfg.GOOGLE_API_TOKEN, no_pages,'',site,mand_word)
            items = [item for sublist in [ans['items'] for ans in answer if 'items' in ans.keys()] for item in sublist]
            items = [item for item in items if item['link'] not in url_list]
            if items:
                items_df = pd.DataFrame(items)
                items_df = prepare_df(items_df)
            else:
                continue
            #collect data from web
            if site==XING:
                items_df = get_xing_data(items_df, summary_)                
            elif site==LINKEDIN:
                items_df = extract_LI_from_google(items_df,skills,summary_)            
            #Remove all without emails
            if cfg.EMAIL in items_df.columns:
                items_df = items_df.dropna(subset=[cfg.EMAIL])
            #skip when empty after removing those without emails
            if items_df.empty:
                continue
            #add search term to items_df
            items_df[cfg.SEARCHQUERY] = [search_term]*len(items_df)
            #get domain name:
            items_df['email_domain'] = items_df[['company']].apply(lambda x: get_domain(x[cfg.COMPANY]),axis = 1)
            
            #collect their emails
            items_df = main_email_collector(items_df,summary_)
            #add emails to source summary
            if summary_:
                summary_.add_source(sources[site_ind], 0, len(items_df))
            #concatenate with the total dataframe so that we later can remove doubles   
            if not all_items_df.empty:
                all_items_df = pd.concat([all_items_df,items_df],axis=0, ignore_index=True)
                #take the duplicates and combine their datas
                aggregation_functions = {col:'first' for col in all_items_df.columns.values}
                all_items_df = all_items_df.groupby([cfg.FIRST_NAME,cfg.LAST_NAME]).aggregate(aggregation_functions)
                url_list =  list(all_items_df['link'])
            else:
                all_items_df = items_df

        #check if the right length
        if len(all_items_df)<no_people:
            ind = ind + 1
            #when we reach the end of the st_list we make a new list
            if ind == len(st_list):
                #if not, shorten the string list
                st_list = st_list[1:]
                ind = 0
            #when there are no more words left, we try with the country name
            if len(st_list)==0 and mand_word==city:
                mand_word = country
                st_list = [w.strip() for w in skills.split(',')]
                ind = 0
            #when we tried with country, we break
            elif len(st_list) ==0 and mand_word != city:
                break
        else:
            #go out of the while when the list is long enough
            break
    
    return all_items_df

def extract_LI_from_google(items_df, skill_str, summary_=None):
    """
    param items_df: data frame with profile data acquired through google_api.google_something
    param skill_str: string with skills seperated by comma's
    param summary_:  summary class object (see find_profiles), that keeps all data concerning the collection process
    and socket information (through which this data can be sent)
    return: data frame with linkedin profile data
    """
    bar = Bar('Collecting LinkedIn profiles: ', max=len(items_df))
    skills = skill_str.split(',')
    #getting skill_set
    items_df[cfg.SKILL_SET] =  items_df[['snippet',cfg.POSTING_TITLE]].apply(lambda x: extract_skills(x['snippet'].lower()+' '+x[cfg.POSTING_TITLE].lower(),skills) if x['snippet'] and x[cfg.POSTING_TITLE] else None,axis = 1)
    #getting work experience text
    items_df[cfg.WORK_EXPERIENCE] = items_df[[cfg.POSTING_TITLE,'company']].apply(lambda x: '\n'.join(['time_period: present', 'position: '+x[cfg.POSTING_TITLE], 'company: '+x['company']]) if x[cfg.POSTING_TITLE] and x['company'] else None, axis = 1)
    #turn all empties to none
    email_domain = [None]*len(items_df)
    for ind in items_df.index:
        bar.next()
        if items_df['company'][ind]==None:
            continue 
        #send update over socket
        if summary_:
            summary_.add_source("linkedin",1)
            summary_.send_message()
        #get domain name:
        company = items_df['company'].iloc[ind]
        domain = get_domain(company)
        email_domain[ind] = domain   
    items_df['email_domain'] = email_domain
    #collect their emails
    items_df = main_email_collector(items_df,summary_)
    return items_df 
    
def get_xing_data(items_df, summary_ = None): 
    """
    param items_df: a dataframe with profile data coming from the google search (google_something) 
    param summary_: summary class object (see find_profiles), that keeps all data concerning the collection process
    and socket information (through which this data can be sent)
    return: items_df with added xing data
    """
    browser = webdriver.Chrome(driver_file)
    browser.set_window_size(1200, 600)
    new_columns = [cfg.CITY,
                   cfg.POSTING_TITLE,
                   cfg.SKILL_SET,
                   cfg.WANTS,
                   cfg.WORK_EXPERIENCE,
                   cfg.EDUCATION,
                   cfg.LANGUAGES,
                   cfg.INTERESTS,
                   cfg.GROUPS,
                   cfg.EVENTS,
                   'similar']    
    df_scrape = items_df[['link',cfg.COMPANY]].apply(lambda x: scrape_xing(x['link'], browser, x[cfg.COMPANY], summary_), axis = 1)    
    df_scrape = df_scrape.apply(pd.Series)
    df_scrape.columns = new_columns
    items_df = items_df.combine_first(df_scrape)
     
    browser.close()

    return items_df

def scrape_xing(url,browser,company=None,summary_ = None):
    """
    param url: XING url
    param browser: selenium webbrowser
    param company: name of the last company the person worked for (can be collected from XING google search, title of page)
    param summary_: summary class object (see find_profiles), that keeps all data concerning the collection process
    return: scraped variables from XING profile that should be stored
    """
    print(url)
    city = None
    job_title = None
    skill_set = None
    wants_set = None
    work_exp = None
    education = None
    languages = None
    interests = None
    groups = None
    events = None
    similars = None
    #isloop is a failsafe for when we can't access the page 
    scrape = True
    #try to access the page
    while True:
        tries = 0
        try:
            tries = tries+1
            browser.get(url)
            break
        except Exception:
            print('error getting page, trying again')
            if tries>5:
                scrape = False
                print(scrape)
                break
            
    #when we can't access the page, we skip
    if scrape:        
        #get the city
        try:
            city_text = browser.find_element_by_xpath('//div[@class="ProfilesvCard-employmentStatus"]').text
            city = city_text[city_text.find('in ')+3:]
        except Exception:
            pass
        
        #get the position
        try:
            job_title = browser.find_element_by_xpath('//span[@class="title ProfilesvCard-jobTitle"]').text
        except Exception:
            pass       
        #get skills
        try:
            skills = browser.find_element_by_xpath('//div[@class="Haves"]')
            skill_set = str({skill.text:None for skill in skills.find_elements_by_xpath('.//li[@class="Tags-item"]')})
        except Exception:
            pass                
        #get wants
        try:
            wants = browser.find_element_by_xpath('//div[@class="Wants"]')
            wants_set = ', '.join([want.text for want in wants.find_elements_by_xpath('.//li[@class="Tags-item"]')])
        except Exception:
            pass
        #get work experience
        try:    
            work_field = browser.find_elements_by_xpath('//div[@class="WorkExperience-description"]')
            if work_field:
                work_exp_data = []
                #loop through work experiences
                for i,work in enumerate(work_field):
                    work_dict = {}
                    fields = [el.get_attribute('class') for el in work.find_elements_by_css_selector("*")]
                    if "WorkExperience-dateRange" in fields:
                        time_period = work.find_element_by_xpath('.//div[@class="WorkExperience-dateRange"]').text
                        date_text = time_period.replace(' ','').split('-')
                        date_text = [dt.split('/')[1]+'-'+dt.split('/')[0]+'-01' if any(char.isdigit() for char in dt) else dt  for dt in date_text ]
                        work_dict['dateRange']={cfg.DATE_FN[ind]:{'isoDate':dt} for ind,dt in enumerate(date_text)}
                    if "WorkExperience-jobTitle" in fields:
                        work_dict['title'] = work.find_element_by_xpath('.//div[@class="WorkExperience-jobTitle"]').text
                    if 'WorkExperience-jobInfo' in fields:
                        company_text = work.find_element_by_xpath('.//div[@class="WorkExperience-jobInfo"]').text
                        work_dict['company'] = company_text.replace(time_period,'').replace(work_dict[i]['title'],'').replace('\n','')
                    work_exp_data.append(work_dict)
                if company:
                    work_exp_data[0]['company'] = company
                work_exp = work_exp_data
            else:
                pass
        except Exception:
            pass
        
        #get education
        try:    
            education_field = browser.find_elements_by_xpath('//div[@class="Educations-description"]')
            if education_field:
                educations_data = []
                #loop through educations
                for i,education_elem in enumerate(education_field):
                    educations_dict = {}
                    fields = [el.get_attribute('class') for el in education_elem.find_elements_by_css_selector("*")]
                    if "Educations-dateRange" in fields:
                         date_text = education_elem.find_element_by_xpath('.//div[@class="Educations-dateRange"]').text.replace(' ','').split('-')
                         date_text = [dt.split('/')[1]+'-'+dt.split('/')[0]+'-01' if any(char.isdigit() for char in dt) else dt  for dt in date_text ]
                         educations_dict['dateRange']={cfg.DATE_FN[ind]:{'isoDate':dt} for ind,dt in enumerate(date_text)}
                    if "Educations-schoolName" in fields:
                       educations_dict['school'] = education_elem.find_element_by_xpath('.//div[@class="Educations-schoolName"]').text
                    if 'Educations-notes' in fields:
                        educations_dict['notes'] = education_elem.find_element_by_xpath('.//div[@class="Educations-notes"]').text
                    educations_data.append(educations_dict)#'\n\n'.join([str(val).replace("', '",'\n') for key,val in educations_dict.items()]).replace("'",'').replace('{','').replace('}','')
                education = educations_data
            else:
                pass
        except Exception:
            pass
        #get languages
        try:
            languages = browser.find_element_by_xpath('//ul[@class="Languages"]').text.replace('\n',', ')
        except Exception:
            pass
        #get interests        
        try:        
            interests_field = browser.find_element_by_xpath('//div[@class="Interests"]')
            interests = ', '.join([interest.text for interest in interests_field.find_elements_by_xpath('.//li[@class="Tags-item"]')])
        except Exception:
            pass
        #get groups
        try:    
            groups_field = browser.find_elements_by_xpath('//a[@data-logged-out-external-action="cvGroupName"]')
            groups = ', '.join([group.text for group in groups_field])        
        except Exception:
            pass
        #get events
        try:
            events_field = browser.find_elements_by_xpath('//a[@data-logged-out-external-action="cvEventName"]')
            events = ', '.join([event.text for event in events_field])
        except Exception:
            pass
        #get similar people:
        try:    
            links = browser.find_element_by_xpath('//ul[@class="SimilarProfiles"]').get_attribute('innerHTML')
            links = links.split('href="')
            links = list(set(['https://www.xing.com'+link[:link.find('"')] for link in links[1:]]))
            similars = links
        except Exception:
            pass
        #send update over socket
        if summary_:
            summary_.add_source("xing",1)
    return city, job_title, skill_set, wants_set, work_exp, education, languages, interests, groups, events, similars
        
if __name__=='__main__':
    XING = 'https://www.xing.com/profile/'
    answer = google_something('java developer','', cfg.GOOGLE_API_TOKEN, 1,'',XING,'Berlin')
    items = [item for sublist in [ans['items'] for ans in answer if 'items' in ans.keys()] for item in sublist]
    items_df =  pd.DataFrame(items)
    #skip when nothing is found
    if items_df.empty == False:
        items_df = prepare_df(items_df)
        #collect data from web
        if items:
            items_df = get_xing_data(items_df) 